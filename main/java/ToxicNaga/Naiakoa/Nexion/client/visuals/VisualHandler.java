package ToxicNaga.Naiakoa.Nexion.client.visuals;


import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.profiler.Profiler;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.opengl.GL11;


/**
 * Created by Naiakoa on 2/6/2015.
 */
public class VisualHandler {
    static double interpPosX;
    static double interpPosY;
    static double interpPosZ;


    @SubscribeEvent
    public void onRenderWorldLast(RenderWorldLastEvent event) {
        Profiler profiler = Minecraft.getMinecraft().mcProfiler;

        profiler.startSection("botania-particles");
        ParticleRenderDispatcher.dispatch();

        float frame = event.partialTicks;
        Entity entity = Minecraft.getMinecraft().thePlayer;

        interpPosX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * frame;
        interpPosY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * frame;
        interpPosZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * frame;

        GL11.glTranslated(-interpPosX, -interpPosY, -interpPosZ);

        GL11.glDepthMask(false);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        ParticleRenderDispatcher.lightningCount = 0;


        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDepthMask(true);

        GL11.glTranslated(interpPosX, interpPosY, interpPosZ);
        profiler.endSection();
        profiler.endSection();

    }
}