package ToxicNaga.Naiakoa.Nexion.client.visuals;

/**
 * Created by Naiakoa on 2/6/2015.
 */

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.ChunkCoordinates;

/**
 * Any TileEntity that implements this is technically bound
 * to something, and the binding will be shown when hovering
 * over with a Wand of the Forest.
 */
public interface ITileBound {

    /**
     * Gets where this block is bound to, can return null.
     */
    @SideOnly(Side.CLIENT)
    public ChunkCoordinates getBinding();

}
