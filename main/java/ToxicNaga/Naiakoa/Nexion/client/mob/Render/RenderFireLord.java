package ToxicNaga.Naiakoa.Nexion.client.mob.Render;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBar;
import ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian.EntityFireLord;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public class RenderFireLord extends RenderLiving {
private static final ResourceLocation EntityTexture = new ResourceLocation("nexion:textures/Boss/FireLord.png");
protected ModelFireLord model;

    public RenderFireLord(ModelBase par1ModelBase, float par2){
        super(par1ModelBase, par2);
        model = ((ModelFireLord)mainModel);
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return EntityTexture;
    }

    public void renderingFireLord(EntityFireLord entity, double par2, double par4, double par6, float par7, float par8){
        super.doRender(entity, par2, par4,par6,par7,par8);
    }

    public void doRenderLiving(EntityLiving entLiving, double par2, double par4, double par6, float par7, float par8){
        renderingFireLord((EntityFireLord)entLiving,par2,par4,par6,par7,par8);
    }

    public void doRender(Entity entity, double par2, double par4, double par6, float par7, float par8){
        EntityFireLord firelord = (EntityFireLord) entity;
        NexionBar.setCurrentBoss(firelord);

        super.doRender(entity, par2, par4, par6, par7, par8);
    }
}
