package ToxicNaga.Naiakoa.Nexion.client.mob.Projectiles.RenderModel;

import ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian.EntityNXFireball;
import net.minecraft.client.model.ModelBase;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

/**
 * Created by Naiakoa on 2/7/2015.
 */
public class RenderFireBall extends RenderLiving {

    private static final ResourceLocation EntityTexture = new ResourceLocation("nexion:textures/Boss/FireBall.png");
    protected ModelFireball model;
    public RenderFireBall(ModelBase par1ModelBase, float par2) {
        super(par1ModelBase, par2);
        setRenderPassModel(new ModelFireball());
        model = ((ModelFireball)mainModel);
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity entity) {
        return EntityTexture;
    }
    @Override
    public void doRender(EntityLiving entityLiving, double par2, double par4, double par6, float par8, float par9) {
        renderingNXFireball((EntityNXFireball) entityLiving, par2, par4, par6, par8, par9);
    }
    public void renderingNXFireball(EntityNXFireball entity, double par2, double par4, double par6, float par7, float par8){
        super.doRender(entity, par2, par4,par6,par7,par8);
    }

    protected int setFireballBrightness(EntityNXFireball fireballEntity, int par2, float par3) {
        if (par2 != 0)
            return -1;
        else {
            bindTexture(getEntityTexture(fireballEntity));
            float f1 = 1.0F;
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glDisable(GL11.GL_ALPHA_TEST);
            GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
            GL11.glDepthMask(true);

            char c0 = 61680;
            int j = c0 % 65536;
            int k = c0 / 65536;
            OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, j / 1.0F, k / 1.0F);
            GL11.glColor4f(1.0F, 0F, 1.0F, 1.0F);
            GL11.glColor4f(1.0F, 0F, 1.0F, f1);
            return 1;
        }
    }

    @Override
    protected int shouldRenderPass(EntityLivingBase par1EntityLivingBase, int par2, float par3) {
        return setFireballBrightness((EntityNXFireball) par1EntityLivingBase, par2, par3);
    }
}