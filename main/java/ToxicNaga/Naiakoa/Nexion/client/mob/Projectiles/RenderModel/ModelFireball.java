package ToxicNaga.Naiakoa.Nexion.client.mob.Projectiles.RenderModel;

import ToxicNaga.Naiakoa.Nexion.Handlers.MathHelper;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
 * Created by Naiakoa on 2/7/2015.
 */
public class ModelFireball extends ModelBase
{
    ModelRenderer Sphere1;
    ModelRenderer SphereUp1;
    ModelRenderer Sphere2;
    ModelRenderer Sphere3;
    ModelRenderer Sphere4;
    ModelRenderer Sphere5;
    ModelRenderer Sphere6;
    ModelRenderer Sphere7;
    ModelRenderer SphereUp2;
    ModelRenderer SphereUp3;
    ModelRenderer SphereUp4;
    ModelRenderer SphereUp5;
    ModelRenderer SphereUp6;

    public ModelFireball()
    {
        textureWidth = 64;
        textureHeight = 32;

        Sphere1 = new ModelRenderer(this, 0, 0);
        Sphere1.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere1.setRotationPoint(0F, 0F, 0F);
        Sphere1.setTextureSize(64, 32);
        Sphere1.mirror = true;
        setRotation(Sphere1, 0F, 0.7435722F, 0F);
        SphereUp1 = new ModelRenderer(this, 0, 0);
        SphereUp1.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp1.setRotationPoint(0F, 0F, 0F);
        SphereUp1.setTextureSize(64, 32);
        SphereUp1.mirror = true;
        setRotation(SphereUp1, 1.375609F, 0F, 0F);
        Sphere2 = new ModelRenderer(this, 0, 0);
        Sphere2.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere2.setRotationPoint(0F, 0F, 0F);
        Sphere2.setTextureSize(64, 32);
        Sphere2.mirror = true;
        setRotation(Sphere2, 0F, 0F, 0F);
        Sphere3 = new ModelRenderer(this, 0, 0);
        Sphere3.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere3.setRotationPoint(0F, 0F, 0F);
        Sphere3.setTextureSize(64, 32);
        Sphere3.mirror = true;
        setRotation(Sphere3, 0F, 0.2230717F, 0F);
        Sphere4 = new ModelRenderer(this, 0, 0);
        Sphere4.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere4.setRotationPoint(0F, 0F, 0F);
        Sphere4.setTextureSize(64, 32);
        Sphere4.mirror = true;
        setRotation(Sphere4, 0F, 0.5576792F, 0F);
        Sphere5 = new ModelRenderer(this, 0, 0);
        Sphere5.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere5.setRotationPoint(0F, 0F, 0F);
        Sphere5.setTextureSize(64, 32);
        Sphere5.mirror = true;
        setRotation(Sphere5, 0F, 1.33843F, 0F);
        Sphere6 = new ModelRenderer(this, 0, 0);
        Sphere6.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere6.setRotationPoint(0F, 0F, 0F);
        Sphere6.setTextureSize(64, 32);
        Sphere6.mirror = true;
        setRotation(Sphere6, 0F, 1.003822F, 0F);
        Sphere7 = new ModelRenderer(this, 0, 0);
        Sphere7.addBox(-3F, -3F, -3F, 6, 6, 6);
        Sphere7.setRotationPoint(0F, 0F, 0F);
        Sphere7.setTextureSize(64, 32);
        Sphere7.mirror = true;
        setRotation(Sphere7, 0F, 0F, 0F);
        SphereUp2 = new ModelRenderer(this, 0, 0);
        SphereUp2.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp2.setRotationPoint(0F, 0F, 0F);
        SphereUp2.setTextureSize(64, 32);
        SphereUp2.mirror = true;
        setRotation(SphereUp2, 0.1858931F, 0F, 0F);
        SphereUp3 = new ModelRenderer(this, 0, 0);
        SphereUp3.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp3.setRotationPoint(0F, 0F, 0F);
        SphereUp3.setTextureSize(64, 32);
        SphereUp3.mirror = true;
        setRotation(SphereUp3, 0.4461433F, 0F, 0F);
        SphereUp4 = new ModelRenderer(this, 0, 0);
        SphereUp4.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp4.setRotationPoint(0F, 0F, 0F);
        SphereUp4.setTextureSize(64, 32);
        SphereUp4.mirror = true;
        setRotation(SphereUp4, 0.7063936F, 0F, 0F);
        SphereUp5 = new ModelRenderer(this, 0, 0);
        SphereUp5.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp5.setRotationPoint(0F, 0F, 0F);
        SphereUp5.setTextureSize(64, 32);
        SphereUp5.mirror = true;
        setRotation(SphereUp5, 0.9294653F, 0F, 0F);
        SphereUp6 = new ModelRenderer(this, 0, 0);
        SphereUp6.addBox(-3F, -3F, -3F, 6, 6, 6);
        SphereUp6.setRotationPoint(0F, 0F, 0F);
        SphereUp6.setTextureSize(64, 32);
        SphereUp6.mirror = true;
        setRotation(SphereUp6, 1.189716F, 0F, 0F);
    }

    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
        super.render(entity, f, f1, f2, f3, f4, f5);
        setRotationAngles(f, f1, f2, f3, f4, f5, entity);
        Sphere1.render(f5);
        SphereUp1.render(f5);
        Sphere2.render(f5);
        Sphere3.render(f5);
        Sphere4.render(f5);
        Sphere5.render(f5);
        Sphere6.render(f5);
        Sphere7.render(f5);
        SphereUp2.render(f5);
        SphereUp3.render(f5);
        SphereUp4.render(f5);
        SphereUp5.render(f5);
        SphereUp6.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
    {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}