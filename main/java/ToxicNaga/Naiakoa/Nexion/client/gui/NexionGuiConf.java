package ToxicNaga.Naiakoa.Nexion.client.gui;

import ToxicNaga.Naiakoa.Nexion.Configuration.ConfigurationHandler;
import ToxicNaga.Naiakoa.Nexion.Configuration.Reference;
import cpw.mods.fml.client.config.GuiConfig;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
public class NexionGuiConf extends GuiConfig
{
    public NexionGuiConf(GuiScreen guiScreen)
    {
        super(guiScreen,
                new ConfigElement(ConfigurationHandler.conf.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
                Reference.MOD_ID,
                false,
                false,
                GuiConfig.getAbridgedConfigPath(ConfigurationHandler.conf.toString()));
    }
}
