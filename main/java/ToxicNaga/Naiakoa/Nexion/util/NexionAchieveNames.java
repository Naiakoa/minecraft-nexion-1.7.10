package ToxicNaga.Naiakoa.Nexion.util;

/**
 * Created by Naiakoa on 2/3/2015.
 */
public class NexionAchieveNames
{
    public static final String WURTZITEFOUND = "wurtzitePickup";
    public static final String MODBEGIN = "theStart";
    public static final String BIODEBEGIN = "Tier2";
    public static final String BIODEFOUND = "biodePickup";
}
