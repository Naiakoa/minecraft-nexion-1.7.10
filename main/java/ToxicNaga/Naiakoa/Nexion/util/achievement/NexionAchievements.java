package ToxicNaga.Naiakoa.Nexion.util.achievement;

import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Configuration.Reference;
import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import ToxicNaga.Naiakoa.Nexion.util.NexionAchieveNames;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;

/**
 * Created by Naiakoa on 2/3/2015.
 */
public class NexionAchievements {

    public static AchievementPage nexionPage;
    public static AchievementPage biodePage;

    public static Achievement startMod;
    public static Achievement wurtzitePickup;

    public static Achievement modTier2;
    public static Achievement biodePickup;

    public static void init() {
        startMod = new AchievementX(NexionAchieveNames.MODBEGIN, 0, 0, new ItemStack(NexionItems.WurtziteElement, 1, 6), null).setSpecial();
        wurtzitePickup = new AchievementX(NexionAchieveNames.WURTZITEFOUND, 1, 1, new ItemStack(NexionBlockRegistry.WurtziteOre, 1, 6), startMod);


        modTier2 = new AchievementX(NexionAchieveNames.BIODEBEGIN, 3, 0, new ItemStack(NexionItems.BiodeElement, 1, 6), null).setSpecial();
        biodePickup = new AchievementX(NexionAchieveNames.BIODEFOUND, 2, 1, new ItemStack(NexionBlockRegistry.BiodeOre, 1, 6), modTier2);



        nexionPage = new AchievementPage("Nexion: Wurtzite", AchievementX.achievements.toArray(new Achievement[AchievementX.achievements.size()]));
        biodePage = new AchievementPage("Nexion: Biode", AchievementB.achievements.toArray(new Achievement[AchievementB.achievements.size()]));
        AchievementPage.registerAchievementPage(nexionPage);
        AchievementPage.registerAchievementPage(biodePage);

        FMLCommonHandler.instance().bus().register(new AchievementTriggerer());
    }
}
