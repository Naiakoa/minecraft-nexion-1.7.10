package ToxicNaga.Naiakoa.Nexion.util.achievement;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;

/**
 * Created by Naiakoa on 2/3/2015.
 */
public interface ICraftAchievement
{
    public Achievement getAchievementOnCraft(ItemStack stack, EntityPlayer player, IInventory matrix);
}