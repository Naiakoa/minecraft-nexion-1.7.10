package ToxicNaga.Naiakoa.Nexion.util.achievement;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.world.World;

/**
 * Created by Naiakoa on 2/3/2015.
 */
public interface IPickupAchievement
{
    public Achievement getAchievementOnPickup(ItemStack stack, EntityPlayer player, EntityItem item);
}