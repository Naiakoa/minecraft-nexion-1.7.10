package ToxicNaga.Naiakoa.Nexion.util.achievement;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Naiakoa on 2/3/2015.
 */
public class AchievementX extends Achievement {

    public static List<Achievement> achievements = new ArrayList();

    public AchievementX(String name, int x, int y, ItemStack icon, Achievement parent){
        super("achievement.nexion:" + name, "nexion:" + name, x, y, icon, parent);
        achievements.add(this);
        registerStat();
    }

    public AchievementX(String name, int x, int y, Item icon, Achievement parent) {
        this(name, x, y, new ItemStack(icon), parent);
    }

    public AchievementX(String name, int x, int y, Block icon, Achievement parent) {
        this(name, x, y, new ItemStack(icon), parent);
    }
}
