package ToxicNaga.Naiakoa.Nexion.CreativeTabs;

import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class NexionTabBlocks extends CreativeTabs
{
	public NexionTabBlocks(String label)
	{
		super(label);
	}
	
	@Override
	public ItemStack getIconItemStack(){
		return new ItemStack(NexionBlockRegistry.WurtziteOre);
	}
	@Override
	public Item getTabIconItem() {
		return getIconItemStack().getItem();
	}
}
