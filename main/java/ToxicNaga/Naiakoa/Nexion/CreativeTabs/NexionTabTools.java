package ToxicNaga.Naiakoa.Nexion.CreativeTabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class NexionTabTools extends CreativeTabs
{
	public NexionTabTools(String label)
	{
		super(label);
	}

	@Override
	public ItemStack getIconItemStack(){
		return new ItemStack(Items.diamond_axe);
	}
	@Override
	public Item getTabIconItem() {
		return getIconItemStack().getItem();
	}
}
