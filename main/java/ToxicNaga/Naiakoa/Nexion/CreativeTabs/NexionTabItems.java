package ToxicNaga.Naiakoa.Nexion.CreativeTabs;

import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class NexionTabItems extends CreativeTabs
{
	public NexionTabItems(String label)
	{
		super(label);
	}

	@Override
	public ItemStack getIconItemStack(){
		return new ItemStack(NexionItems.WurtziteCrystal);
	}
	@Override
	public Item getTabIconItem() {
		return getIconItemStack().getItem();
	}
}
