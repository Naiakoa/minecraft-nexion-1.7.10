package ToxicNaga.Naiakoa.Nexion.CreativeTabs;

import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class NexionTabElements extends CreativeTabs
{
	public NexionTabElements(String label)
	{
		super(label);
	}

	@Override
	public ItemStack getIconItemStack(){
		return new ItemStack(NexionItems.WurtziteElement);
	}
	@Override
	public Item getTabIconItem() {
		return getIconItemStack().getItem();
	}
}
