package ToxicNaga.Naiakoa.Nexion.init;

import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

/**
 * Created by Naiakoa on 1/29/2015.
 */
public class Crafting {
    public static void init(){
        /** Start Item **/
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(NexionItems.DiamondRedstone), "AAA", "ABA", "AAA", 'B', Items.diamond, 'A', Items.redstone));
        
        
        
        
        
        /** Debug Crafting **/
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(NexionItems.WurtziteShard), "CXC", "XBX", "XCX", 'X', "stickWood", 'B',"plankWood", 'C', Items.diamond));
        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(NexionItems.WurtziteDiamondShard), NexionItems.WurtziteShard, new ItemStack(Items.diamond)));//ShapedOreRecipe(new ItemStack(NexionItems.WurtziteShard), "CXC", "XBX", "XCX", 'X', "stickWood", 'B',"plankWood", 'C', Items.diamond));
    }
}
