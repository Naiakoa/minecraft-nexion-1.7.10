package ToxicNaga.Naiakoa.Nexion.Commands;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.DungeonMechanic;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

import java.util.List;
import java.util.Random;

/**
 * Created by Naiakoa on 2/1/2015.
 */
public class ChangeBoolean extends CommandBase {
    private final boolean ENABLED = true;
    public String getCommandName()
    {
        return "change";
    }

    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 2;
    }

    public String getCommandUsage(ICommandSender var)
    {
        return "nexion.cmd.change.useage";
    }

    public void processCommand(ICommandSender var1, String[] var2)
    {
        if (var2.length >= 1 && var2.length <= 2)
        {
            int i = (300 + (new Random()).nextInt(600)) * 20;

            if (var2.length >= 2)
            {
                i = parseIntBounded(var1, var2[1], 1, 1000000) * 20;
            }

            WorldServer worldserver = MinecraftServer.getServer().worldServers[0];

            if ("all".equalsIgnoreCase(var2[0]))
            {
                DungeonMechanic.dungeonComplete = false;
                DungeonMechanic.dungeonFight = false;
                func_152373_a(var1, this, "All are Reset, Dungeon Complete: " + DungeonMechanic.dungeonComplete + " Dungeon Complete: " + DungeonMechanic.dungeonComplete, DungeonMechanic.dungeonComplete);
            }
        }
        else
        {
            throw new WrongUsageException("nexion.cmd.change.useage", new Object[1]);
        }
    }

    /**
     * Adds the strings available in this command to the given list of tab completion options.
     */
    public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_)
    {
        return p_71516_2_.length == 1 ? getListOfStringsMatchingLastWord(p_71516_2_, new String[] {"all"}): null;
    }
}
