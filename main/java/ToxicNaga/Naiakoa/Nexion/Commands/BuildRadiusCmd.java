package ToxicNaga.Naiakoa.Nexion.Commands;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.DungeonFloorDetector;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;

/**
 * Created by Naiakoa on 1/31/2015.
 */
public class BuildRadiusCmd extends CommandBase {
    private static final boolean ENABLED = true;

    public String getCommandName()
    {
        return "buildradius";
    }

    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 2;
    }

    public String getCommandUsage(ICommandSender var1)
    {
        return "nexion.cmd.build.radius";
    }

    public void processCommand(ICommandSender var1, String[] var2)
    {
        if(!ENABLED){var1.addChatMessage(new ChatComponentTranslation("nexion.version.disabled").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));}
        else {
            if (var2.length > 1)
            {
                int i;

                if (var2[0].equals("13x13"))
                {
                    //else if ("rain".equalsIgnoreCase(p_71515_2_[0])
                    if (var2[1].equals("true"))
                    {
                        DungeonFloorDetector.thirteen = true;
                        func_152373_a(var1, this, "nexion.cmd.buildR13.true", new Object[1]);
                    }
                    else if (var2[1].equals("false"))
                    {
                        DungeonFloorDetector.thirteen = false;
                        func_152373_a(var1, this, "nexion.cmd.buildR13.false", new Object[1]);
                    }
                    else
                    {
                        var1.addChatMessage(new ChatComponentTranslation("nexion.cmd.define.buildradius"+ "1").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
                    }

                    return;
                }

                if (var2[0].equals("26x26"))
                {
                    if (var2[1].equals("true"))
                    {
                        DungeonFloorDetector.twentysix = true;
                        func_152373_a(var1, this, "nexion.cmd.buildR26.true",DungeonFloorDetector.twentysix);
                    }
                    else if (var2[1].equals("false"))
                    {
                        DungeonFloorDetector.twentysix = false;
                        func_152373_a(var1, this, "nexion.cmd.buildR26.false", DungeonFloorDetector.twentysix);
                    }
                    else
                    {
                        var1.addChatMessage(new ChatComponentTranslation("nexion.cmd.define.buildradius" + "3").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
                    }
                    return;
                }
            }
        }
        throw new WrongUsageException("nexion.cmd.buildradius.usage", new Object[1]);
    }
}