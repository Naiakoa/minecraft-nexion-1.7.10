package ToxicNaga.Naiakoa.Nexion.Commands;

import ToxicNaga.Naiakoa.Nexion.VersionCheckz.ThreadDownloadMod;
import ToxicNaga.Naiakoa.Nexion.VersionCheckz.VersionChecker;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class DownloadLatestCmd extends CommandBase {
    private static final boolean ENABLED = true;

    @Override
    public String getCommandName() {
        return "download_latest";
    }

    @Override
    public String getCommandUsage(ICommandSender var1) {
        return "/download_latest <version>";
    }

    @Override
    public void processCommand(ICommandSender var1, String[] var2) {
        if(!ENABLED)
            var1.addChatMessage(new ChatComponentTranslation("nexion.version.disabled").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));

        else if(var2.length == 1)
            if(VersionChecker.downloadedFile)
                var1.addChatMessage(new ChatComponentTranslation("nexion.version.downloadedAlready").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
            else if(VersionChecker.startedDownload)
                var1.addChatMessage(new ChatComponentTranslation("nexion.version.downloadingAlready").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
            else new ThreadDownloadMod("Nexion " + var2[0] + ".jar");
    }
}
