package ToxicNaga.Naiakoa.Nexion.Commands;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.DungeonMechanic;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.BlockHarkar;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

import java.util.List;
import java.util.Random;

/**
 * Created by Naiakoa on 2/1/2015.
 */
public class CheckBoolean extends CommandBase {
    private static final boolean ENABLED = true;
    public String getCommandName()
    {
        return "checkvar";
    }

    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 2;
    }

    public String getCommandUsage(ICommandSender var)
    {
        return "nexion.cmd.checkvar.useage";
    }

    public void processCommand(ICommandSender var1, String[] var2)
    {
        if (var2.length >= 1 && var2.length <= 2)
        {
            int i = (300 + (new Random()).nextInt(600)) * 20;

            if (var2.length >= 2)
            {
                i = parseIntBounded(var1, var2[1], 1, 1000000) * 20;
            }

            WorldServer worldserver = MinecraftServer.getServer().worldServers[0];

            if ("dungeoncheck".equalsIgnoreCase(var2[0]))
            {
                func_152373_a(var1, this, "dungeonComplete: " + DungeonMechanic.dungeonComplete, DungeonMechanic.dungeonComplete);
            }
            else if ("dungeonvar".equalsIgnoreCase(var2[0]))
            {
                func_152373_a(var1, this, "dungeonFight: " + DungeonMechanic.dungeonFight, DungeonMechanic.dungeonFight);
            }
            else if("already".equalsIgnoreCase(var2[0]))
            {
                func_152373_a(var1, this, "alreadyExecuted: " + BlockHarkar.alreadyExecuted, BlockHarkar.alreadyExecuted);
            }
        }
        else
        {
            throw new WrongUsageException("nexion.cmd.checkvar.useage", new Object[1]);
        }
    }

    /**
     * Adds the strings available in this command to the given list of tab completion options.
     */
    public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_)
    {
        return p_71516_2_.length == 1 ? getListOfStringsMatchingLastWord(p_71516_2_, new String[] {"dungeoncheck", "dungeonvar", "already"}): null;
    }
}
