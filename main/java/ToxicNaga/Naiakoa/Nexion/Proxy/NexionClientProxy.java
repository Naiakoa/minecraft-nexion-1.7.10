package ToxicNaga.Naiakoa.Nexion.Proxy;

/**
 * Created by Naiakoa on 1/28/2015.
 */

import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Configuration.ConfigurationHandler;
import ToxicNaga.Naiakoa.Nexion.Handlers.HUDHandler;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.ItemHarkarRenderer;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.TileEntityHarkar;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.TileHarkar;
import ToxicNaga.Naiakoa.Nexion.Objects.TileNX;
import ToxicNaga.Naiakoa.Nexion.VersionCheckz.VersionChecker;
import ToxicNaga.Naiakoa.Nexion.client.mob.Projectiles.RenderModel.ModelFireball;
import ToxicNaga.Naiakoa.Nexion.client.mob.Projectiles.RenderModel.RenderFireBall;
import ToxicNaga.Naiakoa.Nexion.client.mob.Render.ModelFireLord;
import ToxicNaga.Naiakoa.Nexion.client.mob.Render.RenderFireLord;
import ToxicNaga.Naiakoa.Nexion.client.visuals.FXBoundry;
import ToxicNaga.Naiakoa.Nexion.client.visuals.VisualHandler;
import ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian.EntityFireLord;
import ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian.EntityNXFireball;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;

public class NexionClientProxy extends NexionCommonProxy
{
    public void init(FMLInitializationEvent event){
        if(ConfigurationHandler.VersionCheckEnabled)
            new VersionChecker().init();

        MinecraftForge.EVENT_BUS.register(new HUDHandler());
        MinecraftForge.EVENT_BUS.register(new VisualHandler());
    }

    @Override
    public void initRenderingAndTextures()
    {
        RenderIds.HarkarRenderId = RenderingRegistry.getNextAvailableRenderId();
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(NexionBlockRegistry.Harkar), new ItemHarkarRenderer());
        ClientRegistry.bindTileEntitySpecialRenderer(TileHarkar.class, new TileEntityHarkar());

        RenderingRegistry.registerEntityRenderingHandler(EntityFireLord.class, new RenderFireLord(new ModelFireLord(), 0.3F));
        RenderingRegistry.registerEntityRenderingHandler(EntityNXFireball.class, new RenderFireBall(new ModelFireball(), 0.3F));
    }

    @Override
    public void handleTileEntityPacket(int x, int y, int z, ForgeDirection orientation, byte state, String customName)
    {
        TileEntity tileEntity = FMLClientHandler.instance().getClient().theWorld.getTileEntity(x, y, z);

        if (tileEntity != null) {
            if (tileEntity instanceof TileNX) {
                ((TileNX) tileEntity).setOrientation(orientation);
                ((TileNX) tileEntity).setState(state);
                ((TileNX) tileEntity).setCustomName(customName);
            }
        }
    }
    private static boolean distanceLimit = true;
    private static boolean depthTest = true;
    @Override
    public void setBoundryFXDistanceLimit(boolean limit) {
        distanceLimit = limit;
    }

    @Override
    public void setBoundryFXDepthTest(boolean test) {
        depthTest = test;
    }
    @Override
    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size, float motionx, float motiony, float motionz, float maxAgeMul) {
        if(!doParticle())
            return;

        FXBoundry bounds = new FXBoundry(world, x, y, z, size, r, g, b, distanceLimit, depthTest, maxAgeMul);
        bounds.motionX = motionx;
        bounds.motionY = motiony;
        bounds.motionZ = motionz;

        Minecraft.getMinecraft().effectRenderer.addEffect(bounds);
    }
    private boolean doParticle() {
        if(!ConfigurationHandler.useDefaultParticleLimits)
            return true;

        float chance = 1F;
        if(Minecraft.getMinecraft().gameSettings.particleSetting == 1)
            chance = 0.6F;
        else if(Minecraft.getMinecraft().gameSettings.particleSetting == 2)
            chance = 0.2F;

        return Math.random() < chance;
    }
    @Override
    public void handleTileWithItemPacket(int x, int y, int z, ForgeDirection orientation, byte state, String customName, int itemID, int metaData, int stackSize, int color) {

        World world = FMLClientHandler.instance().getClient().theWorld;
        TileEntity tileEntity = world.getTileEntity(x, y, z);

        this.handleTileEntityPacket(x, y, z, orientation, state, customName);
    }
}