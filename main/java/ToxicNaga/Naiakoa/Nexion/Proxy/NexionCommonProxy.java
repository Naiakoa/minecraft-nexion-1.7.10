package ToxicNaga.Naiakoa.Nexion.Proxy;

/**
 * Created by Naiakoa on 1/28/2015.
 */
import ToxicNaga.Naiakoa.Nexion.Commands.BuildRadiusCmd;
import ToxicNaga.Naiakoa.Nexion.Commands.ChangeBoolean;
import ToxicNaga.Naiakoa.Nexion.Commands.CheckBoolean;
import ToxicNaga.Naiakoa.Nexion.Commands.DownloadLatestCmd;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import cpw.mods.fml.common.network.IGuiHandler;

public class NexionCommonProxy implements IGuiHandler
{

    public void preInit(FMLPreInitializationEvent event){
        //NexionEntityRegistry.init();
    }

    public void init(FMLInitializationEvent event){

    }
    public void registerKeyBindingHandler() {

    }

    public void registerRenderTickHandler() {

    }
    public void serverStarting(FMLServerStartingEvent event){
        event.registerServerCommand(new DownloadLatestCmd());
        event.registerServerCommand(new BuildRadiusCmd());
        event.registerServerCommand(new CheckBoolean());
        event.registerServerCommand(new ChangeBoolean());
    }

    public void registerDrawBlockHighlightHandler() {

    }

    public void setKeyBinding(String name, int value) {

    }

    public void initRenderingAndTextures() {


    }

    public void registerTileEntities() {

        //GameRegistry.registerTileEntity(TileTurret.class, Strings.TE_CALCINATOR_NAME);
        //GameRegistry.registerTileEntity(TileZeus.class, Strings.TE_Zeus_NAME);
        //GameRegistry.registerTileEntity(TileHarkar.class, Strings.Harkar);
        //GameRegistry.registerTileEntity(TileCluster.class, Strings.Test);
        //GameRegistry.registerTileEntity(TileEntityDemonForge.class,Strings.DemonForge);
        //GameRegistry.registerTileEntity(TileEntityHellForge.class, Strings.HellForge);

    }

    public void sendRequestEventPacket(byte eventType, int originX, int originY, int originZ, byte sideHit, byte rangeX, byte rangeY, byte rangeZ, String data) {

    }

    public void handleTileEntityPacket(int x, int y, int z, ForgeDirection orientation, byte state, String customName) {

    }

    public void handleTileWithItemPacket(int x, int y, int z, ForgeDirection orientation, byte state, String customName, int itemID, int metaData, int stackSize, int color) {

    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world,
                                      int x, int y, int z) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world,
                                      int x, int y, int z) {
        // TODO Auto-generated method stub
        return null;
    }

    public void setBoundryFXDistanceLimit(boolean limit) {
        // NO-OP
    }
    public void setBoundryFXDepthTest(boolean depth) {
        // NO-OP
    }

    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size) {
        BoundryFX(world, x, y, z, r, g, b, size, 0F);
    }

    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size, float gravity) {
        BoundryFX(world, x, y, z, r, g, b, size, gravity, 1F);
    }

    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size, float gravity, float maxAgeMul) {
        BoundryFX(world, x, y, z, r, g, b, size, 0, -gravity, 0, maxAgeMul);
    }

    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size, float motionx, float motiony, float motionz) {
        BoundryFX(world, x, y, z, r, g, b, size, motionx, motiony, motionz, 1F);
    }

    public void BoundryFX(World world, double x, double y, double z, float r, float g, float b, float size, float motionx, float motiony, float motionz, float maxAgeMul) {
        // NO-OP
    }

}
