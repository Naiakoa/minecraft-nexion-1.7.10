package ToxicNaga.Naiakoa.Nexion.VersionCheckz;

/**
 * Created by Naiakoa on 1/28/2015.
 */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import net.minecraftforge.common.MinecraftForge;

public class ThreadVersionChecker extends Thread {

    public ThreadVersionChecker() {
        setName("Nexion Version Checker Thread");
        setDaemon(true);
        start();
    }

    @Override
    public void run() {
        try {
            URL url = new URL("https://dl.dropboxusercontent.com/u/47132467/versions/" + MinecraftForge.MC_VERSION + ".txt");
            BufferedReader r = new BufferedReader(new InputStreamReader(url.openStream()));
            VersionChecker.onlineVersion = r.readLine();
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        VersionChecker.doneChecking = true;
    }
}