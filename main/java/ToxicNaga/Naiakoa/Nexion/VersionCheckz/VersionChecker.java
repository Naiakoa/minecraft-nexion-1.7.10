package ToxicNaga.Naiakoa.Nexion.VersionCheckz;

/**
 * Created by Naiakoa on 1/28/2015.
 */
import ToxicNaga.Naiakoa.Nexion.Nexion;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;

public final class VersionChecker {

    private static final int FLAVOUR_MESSAGES = 50;

    public static boolean doneChecking = false;
    public static String onlineVersion = "";
    public static boolean triedToWarnPlayer = false;

    public static boolean startedDownload = false;
    public static boolean downloadedFile = false;

    public void init() {
        new ThreadVersionChecker();
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void onTick(ClientTickEvent event) {
        if(doneChecking && event.phase == Phase.END && Minecraft.getMinecraft().thePlayer != null && !triedToWarnPlayer) {
            if(!onlineVersion.isEmpty()) {
                EntityPlayer player = Minecraft.getMinecraft().thePlayer;
                int onlineBuild = Integer.parseInt(onlineVersion.split("-")[1]);
                int clientBuild = Nexion.BUILD.contains("GRADLE") ? Integer.MAX_VALUE : Integer.parseInt(Nexion.BUILD);
                if(onlineBuild > clientBuild) {
                    player.addChatComponentMessage(new ChatComponentTranslation("nexion.version.msg" /*+ player.worldObj.rand.nextInt(FLAVOUR_MESSAGES)*/).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE)));
                    player.addChatComponentMessage(new ChatComponentTranslation("nexion.version.outdate", clientBuild, onlineBuild));

                    IChatComponent component = IChatComponent.Serializer.func_150699_a(StatCollector.translateToLocal("nexion.version.updateMessage").replaceAll("%version%", onlineVersion));
                    player.addChatComponentMessage(component);
                }
                if(onlineBuild == clientBuild){
                    player.addChatComponentMessage(new ChatComponentTranslation("nexion.beta.msg").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.LIGHT_PURPLE)));

                    IChatComponent component = IChatComponent.Serializer.func_150699_a(StatCollector.translateToLocal("nexion.version.updateMessage").replaceAll("%version%", onlineVersion));
                    player.addChatComponentMessage(component);
                }
            }

            triedToWarnPlayer = true;
        }
    }

}

