package ToxicNaga.Naiakoa.Nexion.VersionCheckz;

/**
 * Created by Naiakoa on 1/28/2015.
 */
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;

public class ThreadDownloadMod extends Thread {

    String fileName;

    byte[] buffer = new byte[10240];

    int totalBytesDownloaded;
    int bytesJustDownloaded;

    InputStream webReader;

    public ThreadDownloadMod(String fileName) {
        setName("Nexion Download File Thread");
        this.fileName = fileName;

        setDaemon(true);
        start();
    }

    @Override
    public void run() {
        try {
            IChatComponent component = IChatComponent.Serializer.func_150699_a(String.format(StatCollector.translateToLocal("nexion.version.startingDownload"), fileName));
            if(Minecraft.getMinecraft().thePlayer != null)
                Minecraft.getMinecraft().thePlayer.addChatMessage(component);

            VersionChecker.startedDownload = true;

            String base = "https://dl.dropboxusercontent.com/u/47132467/versions/";
            String file = fileName.replaceAll(" ", "%20");
            URL url = new URL(base + file);

            try {
                url.openStream().close(); // Add to DL Counter
            } catch(IOException e) { }

            url = new URL(base + file);
            webReader = url.openStream();

            File dir = new File(".", "mods");
            File f = new File(dir, fileName);
            f.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(f.getAbsolutePath());

            while((bytesJustDownloaded = webReader.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesJustDownloaded);
                buffer = new byte[10240];
                totalBytesDownloaded += bytesJustDownloaded;
            }

            if(Minecraft.getMinecraft().thePlayer != null)
                Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentTranslation("nexion.version.doneDownloading", fileName).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GREEN)));

            Desktop.getDesktop().open(dir);
            VersionChecker.downloadedFile = true;

            outputStream.close();
            webReader.close();
            finalize();
        } catch(Throwable e) {
            e.printStackTrace();
            sendError();
            try {
                finalize();
            } catch(Throwable e1) {
                e1.printStackTrace();
            }
        }
    }

    private void sendError() {
        if(Minecraft.getMinecraft().thePlayer != null)
            Minecraft.getMinecraft().thePlayer.addChatComponentMessage(new ChatComponentTranslation("nexion.version.error").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
    }
}
