package ToxicNaga.Naiakoa.Nexion.Objects;

public class Reference 
{
    public static final boolean DEBUG_MODE = false;
    public static final String MOD_ID = "nexion";
    public static final String ModName = "Nexion Eternal Dimension";
    public static final String CHname = MOD_ID;
    public static final int ShiftIdCorrection = 256;
}