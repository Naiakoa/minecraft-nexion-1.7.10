package ToxicNaga.Naiakoa.Nexion.Objects;
import net.minecraft.util.ResourceLocation;

public class ResourceLocationHelper {

	public static ResourceLocation getResourceLocation(String modId, String path) {
		return new ResourceLocation(modId, path);
	}
	public static ResourceLocation getResourceLocation(String path){
		return new ResourceLocation(Reference.MOD_ID.toLowerCase(), path);
	}
}