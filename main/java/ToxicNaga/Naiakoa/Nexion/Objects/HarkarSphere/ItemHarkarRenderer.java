package ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere;

import ToxicNaga.Naiakoa.Nexion.Objects.Textures;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;


@SideOnly(Side.CLIENT)
public class ItemHarkarRenderer implements IItemRenderer {

    private ModelHarkar modelHarkar;

    public ItemHarkarRenderer() {

        modelHarkar = new ModelHarkar();
    }

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {

        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {

        return true;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {

        switch (type) {
            case ENTITY:{
                renderHarkar(-0.5F, 0.0F, 0.5F);
            }
            case EQUIPPED: {
                renderHarkar(0F, 0.2F, 0F);
                return;
            }
            case EQUIPPED_FIRST_PERSON: {
                renderHarkar(0.0F, 0.2F, 0F);
                return;
            }
            case INVENTORY: {
                renderHarkar(0.4F, 0.2F, 0F);
                return;
            }
            default:
                return;
        }
    }

    private void renderHarkar(float x, float y, float z) {

        GL11.glPushMatrix();

        // Scale, Translate, Rotate
        GL11.glScalef(1F, 1.3F, 1.2F);
        GL11.glTranslatef(x, y, z);

        // Bind texture
        FMLClientHandler.instance().getClient().renderEngine.bindTexture(Textures.Model_Harkar);

        // Render
        modelHarkar.renderPart("Platonic");

        GL11.glPopMatrix();
    }
}
