package ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere;

import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlockModels;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.Proxy.RenderIds;
import ToxicNaga.Naiakoa.Nexion.util.LogHelper;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;

import java.util.Random;
import java.util.Timer;

public class BlockHarkar extends NexionBlockModels
{
    private Random rand = new Random(3);
    public static boolean alreadyExecuted = false;

    static int interval;
    static Timer timer;



    public BlockHarkar() {

        super(Material.rock);
        this.setBlockName(NexionStrings.Harkar);
        this.setHardness(5F);
        this.setBlockBounds(0.1F, 0.0F, 0.1F, 0.9F, 1.0F, 0.9F);
        this.setTickRandomly(true);
    }

    public TileEntity createNewTileEntity(World world, int Metadata) {

        return new TileHarkar();
    }

    private boolean reset()
    {
        alreadyExecuted = false;
        return true;
    }

    @Override
    public boolean renderAsNormalBlock() {

        return false;
    }

    @Override
    public boolean isOpaqueCube() {

        return false;
    }

    @Override
    public int getRenderType() {

        return RenderIds.HarkarRenderId;
    }

    @Override
    public void breakBlock(World world, int x, int y,int z, Block block, int meta) {

        super.breakBlock(world, x, y, z, block, meta);
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        super.onBlockAdded(par1World, par2, par3, par4);
        this.setDefaultDirection(par1World, par2, par3, par4);
        par1World.scheduleBlockUpdate(par2, par3, par4, NexionBlockRegistry.Harkar, 100);
    }

    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
            //int i = rand.nextInt(3);
        //ticks = 1;
        switch(rand.nextInt(3))
        {
            case 1:
            {
                if(!alreadyExecuted)
                {
                    if(!par1World.isRemote) {
                        par5EntityPlayer.addPotionEffect(new PotionEffect(Potion.field_76434_w.id, 1000, 1)); //instant heal
                        par5EntityPlayer.addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 1)); //regeneration
                        par5EntityPlayer.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.DARK_GREEN + StatCollector.translateToLocalFormatted("nexion.harkar.empowered") + " " + EnumChatFormatting.BOLD + EnumChatFormatting.ITALIC + StatCollector.translateToLocalFormatted("nexion.harkar.orb.name")));
                        par5EntityPlayer.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.DARK_GREEN + StatCollector.translateToLocalFormatted("nexion.harkar.chance")));
                        alreadyExecuted = true;
                    }
                    LogHelper.info("Harkar - Case 1 Message & Extra Health/Regeneration");
                }
            }
            case 2:
            {
                if(!alreadyExecuted)
                {   if(!par1World.isRemote) {
                        par5EntityPlayer.addPotionEffect(new PotionEffect(Potion.heal.id, 1500, 1)); //Incase HP by 5 for 2 min
                        par5EntityPlayer.addPotionEffect(new PotionEffect(Potion.regeneration.id, 200, 1)); // Regeneration
                        par5EntityPlayer.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.LIGHT_PURPLE + StatCollector.translateToLocalFormatted("nexion.harkar.empowered") + " " + EnumChatFormatting.BOLD + StatCollector.translateToLocalFormatted("nexion.harkar.orb.name")));
                        alreadyExecuted = true;
                    }
                    LogHelper.info("Harkar - Case 2 Message & Health/Regeneration");
                }
            }
            case 3:
            {
                if(!alreadyExecuted)
                {
                    par5EntityPlayer.addChatComponentMessage(new ChatComponentTranslation(EnumChatFormatting.RED + StatCollector.translateToLocalFormatted("nexion.harkar.lookdown")));
                    alreadyExecuted = true;
                    LogHelper.info("Harkar - Case 3 Message");
                }
            }
        }
        return true;
    }

    private int ticks = 0;
    public void updateTick(World par1World, int x, int y, int z, Random par5Random)
    {
        par1World.scheduleBlockUpdate(x, y, z, NexionBlockRegistry.Harkar, 1);
        if(alreadyExecuted = true)
        {
            ticks ++;
            if(ticks > 20)
            {
                alreadyExecuted = false;
                ticks = 0;
                par1World.setBlockToAir(x, y, z);
                alreadyExecuted = false;
            }
        }
        else
        {
            ticks = 0;
            System.out.println(ticks);
        }
    }

    @Override
    public int tickRate(World par1World)
    {
        if(alreadyExecuted = true)
        {
            return 100;
        }
        else
        {
            return 0;
        }
    }


    private void setDefaultDirection(World world, int x, int y, int z) {
        if(!world.isRemote) {
            Block dir = world.getBlock(x, y, z -1);
            Block dir1 = world.getBlock(x, y, z +1);
            Block dir2 = world.getBlock(x - 1, y, z);
            Block dir3 = world.getBlock(x +1, y, z);
            byte byte0 = 3;

            if(dir.func_149730_j() && !dir1.func_149730_j()) {
                byte0 = 3;
            }

            if(dir1.func_149730_j() && !dir.func_149730_j()) {
                byte0 = 2;
            }

            if(dir2.func_149730_j() && !dir3.func_149730_j()) {
                byte0 = 5;
            }

            if(dir3.func_149730_j() && !dir2.func_149730_j()) {
                byte0 = 4;
            }

            world.setBlockMetadataWithNotify(x, y, z, byte0, 2);
        }
    }
    public static boolean isClient()
    {
        return FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT;
    }

    public static boolean isServer()
    {
        return !isClient();
    }
}
