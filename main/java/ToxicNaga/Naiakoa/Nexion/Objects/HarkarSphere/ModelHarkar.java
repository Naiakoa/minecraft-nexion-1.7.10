package ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere;

import ToxicNaga.Naiakoa.Nexion.Objects.Models;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

@SideOnly(Side.CLIENT)
public class ModelHarkar
{

    private IModelCustom modelHarkar;

    public ModelHarkar() {

        modelHarkar = AdvancedModelLoader.loadModel(Models.HarkarSphere);
    }

    public void render()
    {
        modelHarkar.renderAll();
    }
    public void renderPart(String partName) 
    {
        modelHarkar.renderPart(partName);
    }
}
