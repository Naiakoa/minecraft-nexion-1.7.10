package ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere;

import ToxicNaga.Naiakoa.Nexion.Objects.Textures;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;

/**
 * Created by Naiakoa on 12/22/13.
 */


@SideOnly(Side.CLIENT)
public class TileEntityHarkar extends TileEntitySpecialRenderer {
    private float yRotationAngle;
    private float speed;
    public float Ticks;
    private ModelHarkar modelHarkar = new ModelHarkar();

    @Override
    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick) {

        yRotationAngle += tick * speed;
        speed = 5F;
        Ticks = tick * speed;

        if (tileEntity instanceof TileHarkar) {
            TileHarkar tileHarkar = (TileHarkar) tileEntity;

/*                yRotationAngle ++ tick*speed;*/

            GL11.glPushMatrix();

            // Scale, Translate, Rotate
            GL11.glScalef(1.0F, 1.0F, 1.0F);
            GL11.glTranslatef((float) x + 0.5F, (float) y + 0.5F, (float) z + 0.5F);
            GL11.glRotatef(yRotationAngle, 0F, 0.01F, 0F);

            // Bind texture
            this.bindTexture(Textures.Model_Harkar);
            FMLClientHandler.instance().getClient().renderEngine.bindTexture(Textures.Model_Harkar);

            // Render
            modelHarkar.renderPart("Platonic");

            GL11.glPopMatrix();

        }
    }
}