package ToxicNaga.Naiakoa.Nexion.Objects;

import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

public class Textures
{
	public void test()
	{
    	System.out.println("[Nexion] Loading 3D object textures");
	}
    public static final String ModelTextures = "textures/models/";
//    public static final String ArmorTextures = "textures/armor/";
//    public static final String GUITextures = "textures/gui/";
//    public static final String ZeEffects = "textures/effects/";
//    public static final String BlockTextures = "textures/blocks/";
    public static final ResourceLocation VanillaTexturesBlock = TextureMap.locationBlocksTexture;
    public static final ResourceLocation VanillaTexturesItem = TextureMap.locationItemsTexture;
    public static final ResourceLocation Model_TestBox = ResourceLocationHelper.getResourceLocation(ModelTextures + "turret.png");
    public static final ResourceLocation Model_ZeusBlock = ResourceLocationHelper.getResourceLocation(ModelTextures + "Zeus.png");
    public static final ResourceLocation Model_Test = ResourceLocationHelper.getResourceLocation(ModelTextures + "TestBox.png");
    public static final ResourceLocation Model_Harkar = ResourceLocationHelper.getResourceLocation(ModelTextures + "SphereHarkar.png");
//    public static final ResourceLocation Model_WurtziteOre = ResourceLocationHelper.getResourceLocation(ModelTextures + "wurtziteore.png");
//    public static final ResourceLocation Model_BiodeOre = ResourceLocationHelper.getResourceLocation(ModelTextures + "BiodeOre.png");
//    public static final ResourceLocation Model_NexionOre = ResourceLocationHelper.getResourceLocation(ModelTextures + "NexionOre.png");
//    public static final ResourceLocation GUI_BloodFurnace = ResourceLocationHelper.getResourceLocation(GUITextures + "BloodFurnaceGUI.png");
//    public static final ResourceLocation GUI_MegaChest = ResourceLocationHelper.getResourceLocation(GUITextures + "MegaChest.png");
}