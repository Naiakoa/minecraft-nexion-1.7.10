package ToxicNaga.Naiakoa.Nexion.Objects;

import ToxicNaga.Naiakoa.Nexion.Configuration.Reference;
import net.minecraft.util.ResourceLocation;

public class Models
{
    /* Base file paths */
    public static final String Locations = "models/";

    /* 3D model object names */
    public static final ResourceLocation Turret = ResourceLocationHelper.getResourceLocation(Locations + "Turret.obj");
    public static final ResourceLocation ZeusTwo = ResourceLocationHelper.getResourceLocation(Locations + "ZeusTwo.obj");
    public static final ResourceLocation WurtziteOre = ResourceLocationHelper.getResourceLocation(Locations + "WurtziteOre.obj");
    public static final ResourceLocation Test = ResourceLocationHelper.getResourceLocation(Locations + "Testthree.obj");
    public static final ResourceLocation HarkarSphere = ResourceLocationHelper.getResourceLocation(Locations + "HarkarSphere.obj");
}
