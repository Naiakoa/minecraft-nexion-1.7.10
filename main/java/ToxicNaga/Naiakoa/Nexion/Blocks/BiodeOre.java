package ToxicNaga.Naiakoa.Nexion.Blocks;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

/**
 * Created by Naiakoa on 2/13/2015.
 */
public class BiodeOre extends NexionBlocks {
    public BiodeOre() {
        super(Material.rock);
        this.setBlockName(NexionStrings.BiodeOre);
        this.setCreativeTab(Nexion.nBlocks);
        this.setHardness(15F);
        this.setResistance(3000f);
        this.setHarvestLevel("pickaxe", 3);
    }
}
