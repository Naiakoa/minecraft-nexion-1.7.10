package ToxicNaga.Naiakoa.Nexion.Blocks;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.TileHarkar;
import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;

import java.util.Random;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class WurtziteOre extends NexionBlocks{

    public int flarg;
    public int ItemOne;
    public int ItemTwo;
    Random random = new Random();
    public WurtziteOre(){
        super();
        this.setBlockName(NexionStrings.WurtziteOre);
        this.getUnlocalizedName();
        this.setHardness(10f);
        this.setResistance(3000f);
    }

    public Item getItemDropped(int metadata, Random rand, int fortune){
        flarg = random.nextInt(20 - 1 + 1) + 1;
        System.out.print("||"+flarg+"||");

        if(flarg >= 1 && flarg <= 4 || flarg >= 6 && flarg <= 20){
            return NexionItems.WurtziteFragment;
        }
        if(flarg == 5)
        {
            return NexionItems.WurtziteShard;
        }
        return NexionItems.WurtziteShard;
    }

    public TileEntity createNewTileEntity(World world, int Metadata) {

        return new TileOre();
    }

    public int quantityDropped(Random rand){
        return 2;
    }
    public int getExpDrop(IBlockAccess world, int metadata, int fortune)
    {
        return 2039;
    }
}
