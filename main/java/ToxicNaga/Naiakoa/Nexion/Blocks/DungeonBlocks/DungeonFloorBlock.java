package ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;

/**
 * Created by Naiakoa on 1/31/2015.
 */
public class DungeonFloorBlock extends NexionBlocks
{
    public boolean dungeonComplete = false;
    public DungeonFloorBlock(){
        super();
        this.setBlockName(NexionStrings.groundFloor);
        this.getUnlocalizedName();
        if(dungeonComplete = true){
            this.setHardness(5f);
        }else {
            this.setHardness(300000f);
        }
        this.setResistance(30000000f);
    }
}
