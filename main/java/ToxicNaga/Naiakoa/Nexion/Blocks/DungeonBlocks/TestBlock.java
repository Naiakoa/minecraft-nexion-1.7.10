package ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

/**
 * Created by Naiakoa on 2/6/2015.
 */
public class TestBlock extends NexionBlocks {

    public TestBlock(){
        super();
        this.setBlockName(NexionStrings.dungeonTest);
        this.getUnlocalizedName();
        this.setHardness(300000f);
        this.setResistance(30000000f);
    }


/*    public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer player, int par6, float par7, float par8, float par9) {
        world.setBlock(i, j, k, NexionBlockRegistry.DungeonMechanic);
        return true;
    }*/
}