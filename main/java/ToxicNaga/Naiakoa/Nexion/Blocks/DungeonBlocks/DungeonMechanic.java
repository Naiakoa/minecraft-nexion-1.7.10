package ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks;

import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.util.LogHelper;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.client.event.sound.SoundLoadEvent;

import java.util.Random;
import java.util.Timer;

/**
 * Created by Naiakoa on 2/1/2015.
 */
public class DungeonMechanic extends NexionBlocks {

    private Random rand = new Random(3);
    public static boolean BossDead = false;
    public static boolean dungeonComplete = false;
    public static boolean dungeonFight = false;
    public static boolean triggerOnce = true;
    public static int dmx;
    public static int dmy;
    public static int dmz;


    public DungeonMechanic() {
        super();
        this.setBlockName(NexionStrings.DungeonMechanic);
        this.getUnlocalizedName();
        if (dungeonComplete = true) {
            this.setHardness(5f);
        } else {
            this.setHardness(300000f);
        }
        this.setResistance(30000000f);
    }

    private boolean reset()
    {
        dungeonFight = true;
        return true;
    }

    @Override
    public void breakBlock(World world, int x, int y,int z, Block block, int meta) {

        super.breakBlock(world, x, y, z, block, meta);
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        super.onBlockAdded(par1World, par2, par3, par4);
        par1World.scheduleBlockUpdate(par2, par3, par4, NexionBlockRegistry.DungeonMechanic, 100);
        if(triggerOnce) {
            par1World.playSoundEffect((double) par2, (double) par3, (double) par3, "nexion:nikkitamp3", 3.0F, 1F);
            triggerOnce = false;
        }
    }

    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9, SoundHandler soundHandler, SoundLoadEvent event)
    {
        if(!BossDead) {
            try {
                if (!par1World.isRemote) {
                    par1World.playSoundEffect(par2, par3, par4, "random.levelup", 1.0F, 1.0F);
                    par5EntityPlayer.playSound("random.levelup", 1.0F, 1.0F);
                }
            } catch (Exception e) {
                LogHelper.error(e);
            }
            //int i = rand.nextInt(3);
            //ticks = 1;
            switch (rand.nextInt(3)) {
                case 1: {
                    if (!dungeonFight) {
                        if (!par1World.isRemote) {
                            dungeonFight = true;
                            par1World.playSoundEffect((double) par2 + 0.5D, (double) par3 + 0.5D, (double) par4 + 0.5D, "random.levelup", 3.0F, 13F);
                        }
                    }
                }
                case 2: {
                    if (!dungeonFight) {
                        if (!par1World.isRemote) {
                            dungeonFight = true;
                            par1World.playSoundEffect((double) par2 + 0.5D, (double) par3 + 0.5D, (double) par4 + 0.5D, "random.levelup", 3.0F, 13F);
                        }
                    }
                }
                case 3: {
                    if (!dungeonFight) {
                        dungeonFight = true;
                        par1World.playSoundEffect((double) par2 + 0.5D, (double) par3 + 0.5D, (double) par4 + 0.5D, "random.levelup", 3.0F, 13F);
                    }
                }
            }
        }
        return true;
    }




    private int ticks = 0;
    public void updateTick(World par1World, int i, int j, int k, Random par5Random)
    {
        dmx = i;
        dmy = j;
        dmz = k;
        if(!BossDead) {
            par1World.scheduleBlockUpdate(i, j, k, NexionBlockRegistry.DungeonMechanic, 1);
            if (dungeonFight = true) {
                ticks++;
                if (ticks > 140) {
                    dungeonFight = false;
                    ticks = 0;

                    switch (rand.nextInt(12)) {
                        case 1: {
                            par1World.setBlock(i - 5, j + 2, k + 1, NexionBlockRegistry.Harkar); // Left down
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }
                        case 2: {
                            par1World.setBlock(i - 5, j + 2, k - 1, NexionBlockRegistry.Harkar); // Left Up
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }
                        case 3: {
                            par1World.setBlock(i + 5, j + 2, k - 1, NexionBlockRegistry.Harkar); // Right Up
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }
                        case 4: {
                            par1World.setBlock(i + 5, j + 2, k + 1, NexionBlockRegistry.Harkar);// Right Down
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 5: {
                            par1World.setBlock(i - 1, j + 2, k + 5, NexionBlockRegistry.Harkar); // Up Left
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 6: {
                            par1World.setBlock(i + 1, j + 2, k + 5, NexionBlockRegistry.Harkar); // Up Right
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 7: {
                            par1World.setBlock(i - 1, j + 2, k - 5, NexionBlockRegistry.Harkar); // Down Left
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 8: {
                            par1World.setBlock(i + 1, j + 2, k - 5, NexionBlockRegistry.Harkar); // Down Right
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 9: {
                            par1World.setBlock(i - 8, j + 2, k - 7, NexionBlockRegistry.Harkar); // Top Left Corner
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 10: {
                            par1World.setBlock(i - 8, j + 2, k + 7, NexionBlockRegistry.Harkar); // Bottom Left Corner
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }
                        case 11: {
                            par1World.setBlock(i + 7, j + 2, k - 7, NexionBlockRegistry.Harkar); // Top Right Corner
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                        case 12: {
                            par1World.setBlock(i + 7, j + 2, k + 7, NexionBlockRegistry.Harkar); // Bottom Right Corner
                            par1World.playSoundEffect((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D, "random.levelup", 3.0F, 13F);
                            break;
                        }

                    }
                }
            } else {
                ticks = 200;
                System.out.println(ticks);
            }
        }else if(BossDead){
            par1World.setBlockToAir(i, j, k);
        }
    }

    @Override
    public int tickRate(World par1World)
    {
        if(dungeonFight = true)
        {
            return 100;
        }
        else
        {
            return 0;
        }
    }
    public static boolean isClient()
    {
        return FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT;
    }

    public static boolean isServer()
    {
        return !isClient();
    }
}