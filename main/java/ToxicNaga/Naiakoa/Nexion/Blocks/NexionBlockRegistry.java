package ToxicNaga.Naiakoa.Nexion.Blocks;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.*;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlockModels;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionBlocks;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.BlockHarkar;
import ToxicNaga.Naiakoa.Nexion.Objects.HarkarSphere.TileHarkar;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class NexionBlockRegistry {
    /*Ore*/
    public static final Block WurtziteOre = new WurtziteOre();
    public static final Block BiodeOre = new BiodeOre();

    /*Dungeon Blocks*/
    public static final NexionBlockModels Harkar = new BlockHarkar();
    public static final NexionBlocks groundFloor = new DungeonFloorBlock();
    public static final NexionBlocks summonBlock = new DungeonSummonBlock();
    public static final NexionBlocks DungeonDetector = new DungeonFloorDetector();
    public static final NexionBlocks DungeonMechanic = new DungeonMechanic();
    public static final NexionBlocks TestBlock = new TestBlock();

    public static void init(){
        /*Ore*/
        GameRegistry.registerBlock(WurtziteOre, NexionStrings.WurtziteOre);
        GameRegistry.registerBlock(BiodeOre, NexionStrings.BiodeOre);

        /*Dungeon Blocks*/
        GameRegistry.registerBlock(groundFloor, NexionStrings.groundFloor);
        GameRegistry.registerBlock(summonBlock, NexionStrings.summonBlock);
        GameRegistry.registerBlock(DungeonDetector, NexionStrings.dungeonDetection);
        GameRegistry.registerBlock(TestBlock, NexionStrings.dungeonTest);
        GameRegistry.registerBlock(DungeonMechanic, NexionStrings.DungeonMechanic);
        GameRegistry.registerBlock(Harkar, NexionStrings.Harkar);
        GameRegistry.registerTileEntity(TileHarkar.class, "harkar.TileEntity");
    }
}