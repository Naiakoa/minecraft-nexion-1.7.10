package ToxicNaga.Naiakoa.Nexion.api;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.boss.IBossDisplayData;
import net.minecraft.util.ResourceLocation;

import java.awt.*;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public interface INBoss extends IBossDisplayData {

    /**
     * The ResourceLocation to bind for this boss's boss bar.
     * You can use BotaniaAPI.internalMethodHandler.getDefaultBossBarTexture() to get
     * the one used by botania bosses.
     */
    @SideOnly(Side.CLIENT)
    public ResourceLocation getBBT();

    /**
     * A Rectangle instance delimiting the uv, width and height of this boss's
     * boss bar texture. This is for the background, not the bar that shows
     * the HP.
     */
    @SideOnly(Side.CLIENT)
    public Rectangle getBBTRect();

    /**
     * A Rectangle instance delimiting the uv, width and height of this boss's
     * boss bar HP texture. This is for the foreground that shows how much
     * HP the boss has. The width of the rectangle will be multiplied by the
     * faction of the boss's current HP by max HP.
     */
    @SideOnly(Side.CLIENT)
    public Rectangle getBBHPTRect();

    /**
     * A callback for when this boss's boss bar renders, you can do aditional rendering
     * here if needed.
     */
    @SideOnly(Side.CLIENT)
    public void CallBackBBR();
}
