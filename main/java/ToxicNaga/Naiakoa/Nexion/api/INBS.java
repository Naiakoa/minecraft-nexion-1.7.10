package ToxicNaga.Naiakoa.Nexion.api;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public interface INBS extends INBoss{
    /**
    * @param background True if rendering the background of the boss bar,
    * false if rendering the bar itself that shows the HP.
    */
    @SideOnly(Side.CLIENT)
    public int getBossBarShaderProgram(boolean background);

    /**
     * A callback for the shader, used to pass in uniforms. Return null for no callback.
     */
    @SideOnly(Side.CLIENT)
    public SCB getBossBarShaderCallback(boolean background, int shader);
}
