package ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.DungeonMechanic;
import ToxicNaga.Naiakoa.Nexion.Blocks.NexionBlockRegistry;
import ToxicNaga.Naiakoa.Nexion.Handlers.*;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.api.INBoss;
import ToxicNaga.Naiakoa.Nexion.Handlers.MathHelper;
import ToxicNaga.Naiakoa.Nexion.items.NexionItems;
import ToxicNaga.Naiakoa.Nexion.util.LogHelper;
import ToxicNaga.Naiakoa.Nexion.util.achievement.NexionAchievements;
import ToxicNaga.Naiakoa.Nexion.util.vector.Vector3;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.*;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraftforge.common.util.FakePlayer;

import java.awt.*;
import java.util.List;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public class EntityFireLord extends EntityMob implements INBoss {

    @SideOnly(Side.CLIENT)
    private static Rectangle barRect;
    @SideOnly(Side.CLIENT)
    private static Rectangle hpBarRect;
    private static final float HPMAX = 500F;
    public static final int SPAWN_TICKS = 165;
    public static final int SD_TICKS = 200;
    private static final String TAG_INVUL_TIME = "invulTime";
    private static final String TAG_AGGRO = "aggro";
    private static final String TAG_SOURCE_X = "sourceX";
    private static final String TAG_SOURCE_Y = "sourceY";
    private static final String TAG_SOURCE_Z = "sourcesZ";
    private static final String TAG_IMMUNE_TIME = "immuneTime";
    private static final String TAG_MOB_SPAWN_TICKS = "mobSpawnTicks";
    private static final String TAG_HARD_MODE = "hardMode";

    public static final int MOB_SPAWN_START_TICKS = 20;
    public static final int MOB_SPAWN_END_TICKS = 80;
    public static final int MOB_SPAWN_BASE_TICKS = 800;
    public static final int MOB_SPAWN_TICKS = MOB_SPAWN_BASE_TICKS + MOB_SPAWN_START_TICKS + MOB_SPAWN_END_TICKS;
    public static final int MOB_SPAWN_WAVES = 10;
    public static final int MOB_SPAWN_WAVE_TIME = MOB_SPAWN_BASE_TICKS / MOB_SPAWN_WAVES;

    boolean fireOrbAttack = false;
    private static int tick = 50;
    private static int boundShrink = 100;
    private static int fireRing = 100;

    /**Mechanic Flags**/

    public EntityFireLord(World par1World){
        super(par1World);
        setSize(0.9F, 1.8F);
        getNavigator().setCanSwim(true);
        tasks.addTask(0, new EntityAISwimming(this));
        tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, Float.MAX_VALUE));
        isImmuneToFire = true;
        experienceValue = 5;
        this.findPlayerToAttack();
    }

    public static boolean spawn(EntityPlayer player, ItemStack par1ItemStack, World par3World, int par4, int par5, int par6, boolean hard) {
        DungeonMechanic.BossDead = false;
        Block block = par3World.getBlock(par4, par5, par6);
        if(block == NexionBlockRegistry.TestBlock && !par3World.isRemote) {
            if(par3World.difficultySetting == EnumDifficulty.PEACEFUL) {
                player.addChatMessage(new ChatComponentTranslation("nexion.peacefulFail").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.DARK_RED)));
                return false;
            }

            if(DungeonMechanic.triggerOnce){
                player.worldObj.playSoundAtEntity(player,"nexion:nikkimp3",3F,1F);
            }

            par1ItemStack.stackSize--;
            EntityFireLord e = new EntityFireLord(par3World);
            e.setPosition(par4 + 0.5, par5 + 3, par6 + 0.5);
            par3World.setBlock(par4, par5, par6, NexionBlockRegistry.DungeonMechanic);
            e.setInvulTime(SPAWN_TICKS);
            e.setHealth(1F);
            e.setSource(par4, par5, par6);
            //e.setMobSpawnTicks(MOB_SPAWN_TICKS);
            //e.setHardMode(hard);
            par3World.playSoundAtEntity(e, "mob.enderdragon.growl", 10F, 0.1F);
            par3World.spawnEntityInWorld(e);
            return true;
        }

        return false;
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        dataWatcher.addObject(20, 0); // Invul Time
        dataWatcher.addObject(21, (byte) 0); // Aggro
        dataWatcher.addObject(22, 0); // TP Delay
        dataWatcher.addObject(23, 0); // Source X
        dataWatcher.addObject(24, 0); // Source Y
        dataWatcher.addObject(25, 0); // Source Z
        dataWatcher.addObject(26, 0); // Ticks spawning mobs
        dataWatcher.addObject(27, (byte) 0); // Hard Mode
    }

    public void setInvulTime(int time) {
        dataWatcher.updateObject(20, time);
    }

    public void setSource(int x, int y, int z) {
        dataWatcher.updateObject(23, x);
        dataWatcher.updateObject(24, y);
        dataWatcher.updateObject(25, z);
    }

    public int getInvulTime() {
        return dataWatcher.getWatchableObjectInt(20);
    }
    public boolean isAggored() {
        return dataWatcher.getWatchableObjectByte(21) == 1;
    }
    protected String getLivingSound(){
        return "mob.wither.idle";
    }
    protected String getDeathSound()
    {
        return "mob.wither.death";
    }
    protected Entity findPlayerToAttack(){
        EntityPlayer entityPlayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 16.0D);
        return entityPlayer != null && this.canEntityBeSeen(entityPlayer) ? entityPlayer : null;
    }
    public void applyEntityAttributes(){
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.002D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(40D);
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(HPMAX);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(300000D);
    }
    public ChunkCoordinates getSource() {
        int x = dataWatcher.getWatchableObjectInt(23);
        int y = dataWatcher.getWatchableObjectInt(24);
        int z = dataWatcher.getWatchableObjectInt(25);
        return new ChunkCoordinates(x, y, z);
    }

    @Override
    public void writeEntityToNBT(NBTTagCompound par1nbtTagCompound) {
        super.writeEntityToNBT(par1nbtTagCompound);
        par1nbtTagCompound.setInteger(TAG_INVUL_TIME, getInvulTime());
        par1nbtTagCompound.setBoolean(TAG_AGGRO, isAggored());

        ChunkCoordinates source = getSource();
        par1nbtTagCompound.setInteger(TAG_SOURCE_X, source.posX);
        par1nbtTagCompound.setInteger(TAG_SOURCE_Y, source.posY);
        par1nbtTagCompound.setInteger(TAG_SOURCE_Z, source.posZ);

    }

    @Override
    public void readEntityFromNBT(NBTTagCompound par1nbtTagCompound) {
        super.readEntityFromNBT(par1nbtTagCompound);
        setInvulTime(par1nbtTagCompound.getInteger(TAG_INVUL_TIME));
        setAggroed(par1nbtTagCompound.getBoolean(TAG_AGGRO));

        int x = par1nbtTagCompound.getInteger(TAG_SOURCE_X);
        int y = par1nbtTagCompound.getInteger(TAG_SOURCE_Y);
        int z = par1nbtTagCompound.getInteger(TAG_SOURCE_Z);
        setSource(x, y, z);

    }
    public void setAggroed(boolean aggored) {
        dataWatcher.updateObject(21, (byte) (aggored ? 1 : 0));
    }


    @Override
    public boolean attackEntityFrom(DamageSource par1DamageSource, float par2) {
        if((par1DamageSource.damageType.equals("player") || par1DamageSource.getEntity() instanceof EntityNXFireball) && par1DamageSource.getEntity() != null && !(isTruePlayer(par1DamageSource.getEntity())) && getInvulTime() == 0)
            return super.attackEntityFrom(par1DamageSource, par2 * (1F));
        return false;
    }

    public boolean isTruePlayer(Entity e) {
        if(e instanceof EntityPlayer)
            return false;
        if(worldObj.isRemote)
            return true;

        EntityPlayer player = (EntityPlayer) e;

        String name = player.getCommandSenderName();
        return !(player instanceof FakePlayer || name.contains("[CoFH]") || name.contains("[Thaumcraft"));
    }

    @Override
    public void onLivingUpdate() { /**Boss Fight Mechanics**/
        super.onLivingUpdate();
        boolean hard = isHardMode();
        if (!worldObj.isRemote && worldObj.difficultySetting == EnumDifficulty.PEACEFUL)
            setDead();
        ChunkCoordinates source = getSource();

        boolean Health = getHealth() / getMaxHealth() < 0.5;

        float range = 100F;
        List<EntityPlayer> players = worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.getBoundingBox(source.posX + 0.5 - range, source.posY + 0.5 - range, source.posZ + 0.5 - range, source.posX + 0.5 + range, source.posY + 0.5 + range, source.posZ + 0.5 + range));

        range = 15F;
        for (int i = 0; i < 360; i += 8) {
            float r = 1F;
            float g = 0F;
            float b = 0F;
            float m = 0.15F;
            float mv = 0.35F;

            float rad = i * (float) Math.PI / 180F;
            double x = source.posX - 0.5 - Math.cos(rad) * range;
            double y = source.posY - 0.5;
            double z = source.posZ - 0.5 - Math.sin(rad) * range;

            Nexion.proxy.BoundryFX(worldObj, x, y, z, r, g, b, 0.5F, (float) (Math.random() - 0.5F) * m, (float) (Math.random() - 0.5F) * mv, (float) (Math.random() - 0.5F) * m);
        }

        if (players.isEmpty() && !worldObj.playerEntities.isEmpty())
            setDead();
        else {
            for (EntityPlayer player : players) {

                player.capabilities.isFlying = player.capabilities.isFlying && player.capabilities.isCreativeMode;


                /**Arena Boundries**/
                if (MathHelper.pointDistanceSpace(player.posX, player.posY, player.posZ, source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5) >= range) {
                    Vector3 sourceVector = new Vector3(source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5);
                    Vector3 playerVector = Vector3.fromEntityCenter(player);
                    Vector3 motion = sourceVector.copy().sub(playerVector).copy().normalize();

                    player.motionX = motion.x;
                    player.motionY = 0.7;
                    player.motionZ = motion.z;
                }
            }
        }

        if (isDead)
            return;

        int invul = getInvulTime();
        int mobTicks = 1;

        if (invul > 0) {
            if (invul < SPAWN_TICKS && invul > SPAWN_TICKS / 2 && worldObj.rand.nextInt(SPAWN_TICKS - invul + 1) == 0)
                for (int i = 0; i < 2; i++)
                    spawnExplosionParticle();

            setHealth(getHealth() + (HPMAX - 1F) / SPAWN_TICKS);
            setInvulTime(invul - 1);
            LogHelper.info(invul);
            motionY = 0;

        } else {

            if (isAggored()) {
                boolean dying = getHealth() / getMaxHealth() < 0.2;

                /**Begin Boss position root**/
                //LogHelper.info("X: " + posX + ", Y: " + posY + ", Z:" + posZ);
                LogHelper.info("PosX: " + posX + ", PosZ: " + posZ);
                posX = source.posX+ 0.5;
                posZ = source.posZ + 0.5;
                /**End Boss position root**/

                if (getHealth() > 300) { //Every time the boss is attacked...corner blocks spit a fireball at player (Currently for temp the boss shoots it)
                    if (fireOrbAttack) {
                        tick--;
                        if (tick == 0) {
                            for (int i = 0; i < (fireOrbAttack ? worldObj.rand.nextInt(hard ? 6 : 3) : 1); i++) {
                                EntityNXFireball fireball = new EntityNXFireball(worldObj);
                                fireball.setProps(players.get(rand.nextInt(players.size())), this, 1, 8);
                                fireball.setPosition(posX + width / 2, posY, posZ + width / 2);
                                worldObj.spawnEntityInWorld(fireball);
                                fireOrbAttack = false;
                                tick = 50;
                            }
                        }
                    }
                } else if (getHealth() > 200 && getHealth() < 299) { //Immune to damage for 7 seconds, Flings the players arround
                    setTPDelay(10);
                    for (EntityPlayer player : players) {

                        boundShrink--;
                        if (boundShrink < 10) {
                            for (int i = 0; i < 360; i += 8) {
                                float r = 0F;
                                float g = 0F;
                                float b = 1F;
                                float m = 0.15F;
                                float mv = 0.35F;

                                float rad = i * (float) Math.PI / 180F;
                                double x = source.posX - 0.5 - Math.cos(rad) * boundShrink;
                                double y = source.posY - 0.5;
                                double z = source.posZ - 0.5 - Math.sin(rad) * boundShrink;

                                Nexion.proxy.BoundryFX(worldObj, x, y, z, r, g, b, 0.5F, (float) (Math.random() - 0.5F) * m, (float) (Math.random() - 0.5F) * mv, (float) (Math.random() - 0.5F) * m);
                            }


                            /**Bouncy Ball Ring**/
                            if (MathHelper.pointDistanceSpace(player.posX, player.posY, player.posZ, source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5) >= boundShrink) {
                                Vector3 sourceVector = new Vector3(source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5);
                                Vector3 playerVector = Vector3.fromEntityCenter(player);
                                Vector3 motion = sourceVector.copy().sub(playerVector).copy().normalize();

                                player.motionX = motion.x * 2;
                                player.motionY = 0.7;
                                player.motionZ = motion.z * 2;
                                boundShrink = 50;
                            }
                        }
                    }
                } else if(getHealth() > 10 && getHealth() < 199){ //send off a fire damaging ring ever 2 seconds

                    fireRing--;
                    LogHelper.info("Fire Ring: " + fireRing);
                    if(fireRing < 25) {
                        for (EntityPlayer player : players) {
                            for (int i = 0; i < 360; i += 8) {
                                float r = 1F;
                                float g = 0.5F;
                                float b = 0F;
                                float m = 0.15F;
                                float mv = 0.35F;

                                float rad = i * (float) Math.PI / 180F;
                                double x = source.posX - 0.5 - Math.cos(rad) * fireRing;
                                double y = source.posY - 0.5;
                                double z = source.posZ - 0.5 - Math.sin(rad) * fireRing;

                                Nexion.proxy.BoundryFX(worldObj, x, y, z, r, g, b, 0.5F, (float) (Math.random() - 0.5F) * m, (float) (Math.random() - 0.5F) * mv, (float) (Math.random() - 0.5F) * m);
                            }

                            /**Flame Ring Ring**/
                            if (MathHelper.pointDistanceSpace(player.posX, player.posY, player.posZ, source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5) >= fireRing) {
                                if(fireRing <= 0) {
                                    fireRing = 120;
                                }
                                if (!player.isBurning()) {
                                    player.setFire(4);
                                }
                            }
                        }
                    }
                }

                if (dying && mobTicks > 0) {
                    motionX = 0;
                    motionY = 0;
                    motionZ = 0;
                }




            } else if (getTPDelay() > 0 && !worldObj.isRemote) {
                if (invul > 0)
                    setInvulTime(invul - 1);

                setTPDelay(getTPDelay() - 1);
            }
        }

        if (getHealth() > 10)
        {
            for (EntityPlayer player : players) {
                for (int i = 0; i < 360; i += 8) {
                    float r = 1F;
                    float g = 0.5F;
                    float b = 0F;
                    float m = 0.15F;
                    float mv = 0.35F;

                    float rad = i * (float) Math.PI / 180F;
                    double x = source.posX + 0.5;
                    double y = source.posY - 0.3;
                    double z = source.posZ + 0.5;

                    Nexion.proxy.BoundryFX(worldObj, x, y, z, r, g, b, 0.5F, (float) (Math.random() - 0.5F) * m, (float) (Math.random() - 0.5F) * mv, (float) (Math.random() - 0.5F) * m);
                }
                if (MathHelper.pointDistanceSpace(player.posX, player.posY, player.posZ, source.posX + 0.5, source.posY + 0.5, source.posZ + 0.5) <= 1) {
                    if(!player.isBurning()) {
                        player.setFire(5);
                    }
                }
            }
        }
    }

    @Override
    protected void damageEntity(DamageSource par1DamageSource, float par2) {
        super.damageEntity(par1DamageSource, par2);

        Entity attacker = par1DamageSource.getEntity();
        if(attacker != null) {
            Vector3 thisVector = Vector3.fromEntityCenter(this);
            Vector3 playerVector = Vector3.fromEntityCenter(attacker);
            Vector3 motionVector = thisVector.copy().sub(playerVector).copy().normalize().multiply(0.75);

            if(getHealth() > 0){
                fireOrbAttack = isAggored();
            }

            setAggroed(true);
        }
    }

    @Override
    public void setHealth(float p_70606_1_) {
        super.setHealth(p_70606_1_);
    }
    public int getTotalArmorValue()
    {
        return 10;
    }
    protected boolean isAIEnabled()
    {
        return true;
    }
    @SideOnly(Side.CLIENT)
    public float getShadowSize()
    {
        return this.height / 8.0F;
    }
    public void setMobSpawnTicks(int ticks) {
        dataWatcher.updateObject(26, ticks);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public ResourceLocation getBBT(){
        return NexionBar.defaultHPbar;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public Rectangle getBBTRect(){
        if(barRect == null){barRect = new Rectangle(0, 0, 185, 15);}
        return barRect;
    }
    @Override
    @SideOnly(Side.CLIENT)
    public Rectangle getBBHPTRect(){
        if(hpBarRect == null){hpBarRect = new Rectangle(0, barRect.y + barRect.height, 181, 7);}
        return hpBarRect;
    }
    @Override
    @SideOnly(Side.CLIENT)
    public void CallBackBBR(){}

    @Override
    protected void dropFewItems(boolean par1, int par2){
        if(par1) {
            boolean hard = isHardMode();
            entityDropItem(new ItemStack(NexionItems.BiodeElement, 1), 1F);
        }
    }

    @Override
    public void onDeath(DamageSource p_70645_1_) {
        super.onDeath(p_70645_1_);
        EntityLivingBase entitylivingbase = func_94060_bK();
        if(entitylivingbase instanceof EntityPlayer) {
            ((EntityPlayer) entitylivingbase).addStat(NexionAchievements.modTier2, 1);
        }
        DungeonMechanic.BossDead = true;
        DungeonMechanic.triggerOnce = true;
    }
    public int getTPDelay() {
        return dataWatcher.getWatchableObjectInt(22);
    }
    public void setTPDelay(int delay) {
        dataWatcher.updateObject(22, delay);
    }
    public boolean isHardMode() {
        return dataWatcher.getWatchableObjectByte(27) == 1;
    }

    public int tickRate(World par1World) {
        return 100;
    }
}
