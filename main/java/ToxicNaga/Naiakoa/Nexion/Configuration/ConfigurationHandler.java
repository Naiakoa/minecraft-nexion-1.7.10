package ToxicNaga.Naiakoa.Nexion.Configuration;

import cpw.mods.fml.client.config.DummyConfigElement;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.FMLLog;

import java.io.File;
import java.util.logging.Level;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class ConfigurationHandler {

    public static Property seenVersion;
    public static int Test1;
    public static boolean lore1 = false;
    public static boolean HighQualityPortal = false;
    public static Configuration conf;
    public static final String CATEGORY_BLOCK_PROPERTIES = Configuration.CATEGORY_GENERAL;//.CATEGORY_BLOCK;;
    public static boolean VersionCheckEnabled = true;
    public static boolean useShaders = true;
    public static boolean matrixMode = false;
    public static boolean useDefaultParticleLimits = true;


    public static void init(File configFile)
    {
        //create config
        if(conf == null) {
            conf = new Configuration(configFile);
            loadConfiguration();
        }
    }

    private static void loadConfiguration(){


        conf.addCustomCategoryComment(CATEGORY_BLOCK_PROPERTIES, "These following settings are changeable ingame, please be aware they might be a tad glitchy");
        conf.addCustomCategoryComment("Visual Settings", "Want more visualization? Change these settings to your desire!");
        conf.addCustomCategoryComment("Lore Enable", "Set these to true if for lore levels........*cough*cheater*cough*");

        lore1 = conf.getBoolean("configValue", Configuration.CATEGORY_GENERAL, false, "This is an example configuration value");
        HighQualityPortal = conf.getBoolean("Quality Portals", "Visual Settings", false, "Enable this for 3D Rendered Multiblock walls");
        Test1 = conf.getInt("LoreEnabled", Configuration.CATEGORY_GENERAL, 1, 0, 255, "Testing Int Values");
        useShaders = conf.getBoolean("Shaders", Configuration.CATEGORY_GENERAL, true, "Disable to increase performance and disable shaders");
        matrixMode = conf.getBoolean("MatrixMode", Configuration.CATEGORY_GENERAL, false, "Visually Entertaining");
        useDefaultParticleLimits = conf.getBoolean("Default PartLimit", Configuration.CATEGORY_GENERAL, true, "Use boring Defaultness");

        if(conf.hasChanged()){
            conf.save();
        }
    }
    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if (event.modID.equalsIgnoreCase(Reference.MOD_ID))
        {
            loadConfiguration();
        }
    }
}