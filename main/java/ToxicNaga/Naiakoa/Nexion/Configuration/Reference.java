package ToxicNaga.Naiakoa.Nexion.Configuration;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class Reference
{
    public static final boolean DEBUG_MODE = false;
    public static final String MOD_ID = "nexion";
    public static final String ModName = "Nexion Eternal Dimension";
    public static final String CHname = MOD_ID;
    public static final String GUI_FACTORY_CLASS = "ToxicNaga.Naiakoa.Nexion.client.gui.FactGui";
}