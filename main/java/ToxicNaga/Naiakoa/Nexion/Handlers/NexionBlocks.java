package ToxicNaga.Naiakoa.Nexion.Handlers;

import ToxicNaga.Naiakoa.Nexion.Configuration.Reference;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class NexionBlocks extends Block {

    public NexionBlocks(Material material){
        super(material);
    }
    public NexionBlocks(){
        this(Material.rock);
        this.setCreativeTab(Nexion.nBlocks);
    }
    @Override
    public String getUnlocalizedName()
    {
        return String.format("tile.%s%s", Reference.MOD_ID.toLowerCase() + ":", getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister)
    {
        blockIcon = iconRegister.registerIcon(String.format("%s", getUnwrappedUnlocalizedName(this.getUnlocalizedName())));
    }

    protected String getUnwrappedUnlocalizedName(String unlocalizedName)
    {
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }
}
