package ToxicNaga.Naiakoa.Nexion.Handlers;

/**
 * Created by Naiakoa on 1/30/2015.
 */
import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
public class AchievementHandler extends Achievement
{
    public static List<Achievement> achievements = new ArrayList();
    public AchievementHandler(String name, int x, int y, ItemStack icon, Achievement parent)
    {
        super("achievement.botania:" + name, "botania:" + name, x, y, icon, parent);
        achievements.add(this);
        registerStat();
    }
    public AchievementHandler(String name, int x, int y, Item icon, Achievement parent)
    {
        this(name, x, y, new ItemStack(icon), parent);
    }
    public AchievementHandler(String name, int x, int y, Block icon, Achievement parent)
    {
        this(name, x, y, new ItemStack(icon), parent);
    }
}
