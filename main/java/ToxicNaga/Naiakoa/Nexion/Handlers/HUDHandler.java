package ToxicNaga.Naiakoa.Nexion.Handlers;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.profiler.Profiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public class HUDHandler {
    @SubscribeEvent
    public void onDrawScreen(RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getMinecraft();
        Profiler profiler = mc.mcProfiler;
        ItemStack equippedStack = mc.thePlayer.getCurrentEquippedItem();

        if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
            profiler.startSection("nexion-hud");
            MovingObjectPosition pos = mc.objectMouseOver;

            if (pos != null) {
                Block block = mc.theWorld.getBlock(pos.blockX, pos.blockY, pos.blockZ);
                TileEntity tile = mc.theWorld.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);

                profiler.endStartSection("bossBar");
                NexionBar.render(event.resolution);
                profiler.endSection();
                profiler.endSection();

                GL11.glColor4f(1F, 1F, 1F, 1F);
            }
        }
    }
}
