package ToxicNaga.Naiakoa.Nexion.Handlers;

import ToxicNaga.Naiakoa.Nexion.Objects.ResourceLocationHelper;
import ToxicNaga.Naiakoa.Nexion.api.INBS;
import ToxicNaga.Naiakoa.Nexion.api.INBoss;
import ToxicNaga.Naiakoa.Nexion.api.SCB;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public class NexionBar {
    public static final ResourceLocation defaultHPbar = new ResourceLocation(NexionStrings.GUIHP_BAR);
    static INBoss currentBoss;

    private static final BarCallback barUniformCallback = new BarCallback();

    public static void setCurrentBoss(INBoss status) {
        currentBoss = status;
    }

    public static void render(ScaledResolution res) {
        if(currentBoss == null)
            return;

        Minecraft mc = Minecraft.getMinecraft();
        Rectangle bgRect = currentBoss.getBBTRect();
        Rectangle fgRect = currentBoss.getBBHPTRect();
        String name = currentBoss.func_145748_c_().getFormattedText();
        int c = res.getScaledWidth() / 2;
        int x = c - bgRect.width / 2;
        int y = 30;
        int xf = x + (bgRect.width - fgRect.width) / 2;
        int yf = y + (bgRect.height - fgRect.height) / 2;
        int fw = (int) ((double) fgRect.width * (currentBoss.getHealth() / currentBoss.getMaxHealth()));
        int tx = c - mc.fontRenderer.getStringWidth(name) / 2;

        GL11.glColor4f(1F, 1F, 1F, 1F);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        currentBoss.CallBackBBR();
        mc.renderEngine.bindTexture(currentBoss.getBBT());
        drawBar(x, y, bgRect.x, bgRect.y, bgRect.width, bgRect.height, true);
        drawBar(xf - 2, yf - 5, fgRect.x, fgRect.y - 1, fw, fgRect.height, false);
        mc.fontRenderer.drawStringWithShadow(name, tx, y - 10, 0xFF00EE);
        GL11.glDisable(GL11.GL_BLEND);

        Entity e = (Entity) currentBoss;
        EntityPlayer p = mc.thePlayer;
        if(e.isDead || !p.worldObj.loadedEntityList.contains(e) || MathHelper.pointDistanceSpace(e.posX, e.posY, e.posZ, p.posX, p.posY, p.posZ) > 32)
            currentBoss = null;
    }

    public static void drawBar(int x, int y, int u, int v, int w, int h, boolean bg) {

        boolean useShader = currentBoss instanceof INBS;
        if(useShader) {
            INBS shader = (INBS) currentBoss;
            int program = shader.getBossBarShaderProgram(bg);
            SCB callback = program == 0 ? null : shader.getBossBarShaderCallback(bg, program);
            barUniformCallback.set(u, v, callback);

            ShaderHelper.useShader(program, barUniformCallback);
        }

        //drawTexturedModalRect(x, y, 0, u, v, w, h);
        drawTexturedModalRect(x, y, 0, u, v, w, 14);

        if(useShader)
            ShaderHelper.releaseShader();
    }

    static class BarCallback extends SCB {
        int x, y;
        SCB callback;

        @Override
        public void call(int shader) {
            int startXUniform = ARBShaderObjects.glGetUniformLocationARB(shader, "startX");
            int startYUniform = ARBShaderObjects.glGetUniformLocationARB(shader, "startY");


            ARBShaderObjects.glUniform1iARB(startXUniform, x);
            ARBShaderObjects.glUniform1iARB(startYUniform, y);

            if(callback != null)
                callback.call(shader);
        }

        void set(int x, int y, SCB callback) {
            this.x = x;
            this.y = y;
            this.callback = callback;
        }
    }

    public static void drawTexturedModalRect(int par1, int par2, float z, int par3, int par4, int par5, int par6) {
        //(x, y, 0, u, v, w, h);
        //x y z, par3, par4, par5, par6, width, height
        drawTexturedModalRect(par1, par2, z, par3, par4, par5, par6, 0.00542F, 0.00389F);
        //drawTexturedModalRect(par1, 14, z, par3, par4, par5, par6, 0.00540313F, 0.0035F);
        //drawTexturedModalRect(par1, par2, z, par3, par4, par5, par6, 0.00390625F, 0.00390625F);
    }

    public static void drawTexturedModalRect(int par1, int par2, float z, int par3, int par4, int par5, int par6, float f, float f1) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(par1 + 0, par2 + par6, z, (par3 + 0) * f, (par4 + par6) * f1);
        tessellator.addVertexWithUV(par1 + par5, par2 + par6, z, (par3 + par5) * f, (par4 + par6) * f1);
        tessellator.addVertexWithUV(par1 + par5, par2 + 0, z, (par3 + par5) * f, (par4 + 0) * f1);
        tessellator.addVertexWithUV(par1 + 0, par2 + 0, z, (par3 + 0) * f, (par4 + 0) * f1);
        tessellator.draw();
    }
}
