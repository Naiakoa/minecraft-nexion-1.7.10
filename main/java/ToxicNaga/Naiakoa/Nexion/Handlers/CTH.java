package ToxicNaga.Naiakoa.Nexion.Handlers;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

/**
 * Created by Naiakoa on 2/4/2015.
 */
public class CTH {
    public static int pageFlipTicks = 0;
    public static int ticksInGame = 0;
    public static float partialTicks = 0;

    @SubscribeEvent
    public void renderTickStart(TickEvent.RenderTickEvent event) {
        if(event.phase == TickEvent.Phase.START)
            partialTicks = event.renderTickTime;
    }

    @SubscribeEvent
    public void clientTickEnd(TickEvent.ClientTickEvent event) {
        if(event.phase == TickEvent.Phase.END) {

            GuiScreen gui = Minecraft.getMinecraft().currentScreen;
            if(gui == null || !gui.doesGuiPauseGame()) {
                ticksInGame++;
            }
        }
    }

    public static void notifyPageChange() {
        if(pageFlipTicks == 0)
            pageFlipTicks = 5;
    }

}
