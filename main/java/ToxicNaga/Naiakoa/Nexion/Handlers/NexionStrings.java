package ToxicNaga.Naiakoa.Nexion.Handlers;

import ToxicNaga.Naiakoa.Nexion.Configuration.Reference;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class NexionStrings {
        /* General keys */

    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TOKEN_DELIMITER = ".";

    /* Asset Locations */
    public static String PREFIXID = "nexion:";
    public static final String ASSET_GUI = PREFIXID + "textures/gui/";
    public static final String ASSET_VISUAL = PREFIXID + "textures/visuals/";
    public static final String ASSET_MODLETEXT = PREFIXID + "textures/entity/";


    /* GUI */
    public static final String GUIHP_BAR = ASSET_GUI + "bossHPBar.png";

    /* Block name constants */
    public static final String Turret_NAME = "turret";
    public static final String Zeus_Name = "zeus";
    public static final String WurtziteOre = "WurtziteOre";
    public static final String BiodeOre = "biodeore";
    public static final String NexionOre = "nexionore";
    public static final String DemonChestx = "DemonChest";
    public static final String DemonForge = "DemonForge";
    public static final String HellForge = "HellForge";
    public static final String SlabGlass = "GlassSlab";
    public static final String SlabGlassDouble = "GlassSlabDouble";
    public static final String Test = "Testing";
    public static final String Harkar = "HarkarSphere";

    /* Item name constants */
    public static final String RedDiamond = "RedDiamond";
    public static final String Wurtzite_Fragment = "WurtziteFragment";
    public static final String Wurtzite_Shard = "WurtziteShard";
    public static final String Wurtzite_Crystal = "WurtziteCrystal";
    public static final String Wurtzite_Element = "WurtziteElement";
    public static final String Wurtzite_Sowrd = "WurtziteSword";
    public static final String Wurtzite_Pickaxe = "WurtzitePickaxe";
    public static final String WurtziteDiamondShard = "WurtziteDiamondShard";
    public static final String Biode_Ingot = "BiodeIngot";
    public static final String Biode_Sword = "BiodeSword";
    public static final String Biode_Shovel = "BiodeShovel";
    public static final String Biode_Element = "BiodeElement";
    public static final String Nexion_Ingot = "NexionIngot";
    public static final String Nexion_Element = "NexionElement";
    public static final String Z_Element = "ZElement";
    public static final String Wurtzite_Sword = "WurtziteSword";
    public static final String God_Sword = "GodSword";
    public static final String MultiShardWurt = "WurtziteShardBundle";
    public static final String LoreMain = "LoreBook";

    /* Dungeon Blocks */
    public static final String groundFloor = "DungeonFloor";
    public static final String summonBlock = "DungeonSummon";
    public static final String dungeonDetection = "DungeonFloorDetect";
    public static final String DungeonMechanic = "DungeonLogic";
    public static final String dungeonTest = "DungeonTest";

    /* TileEntity name constants */
    public static final String TE_CALCINATOR_NAME = "Turret";
    public static final String CONTAINER_CALCINATOR_NAME = "container." + Turret_NAME;
    public static final String BloodFurnace_Name = "BloodFurnace";

    public static final String TE_Zeus_NAME = "Zeus";
    public static final String CONTAINER_Zeus_NAME = "container." + Zeus_Name;

    /* Entity Names */
    public static final String FIRE_LORD ="fireLord";
    public static final String FireBallEnt = "FireBall";
    public static final String Fireball = ASSET_MODLETEXT + "FireBall.png";

    /* Visuals */
        public static final String fireVisual = ASSET_VISUAL + "fire.png";


}
