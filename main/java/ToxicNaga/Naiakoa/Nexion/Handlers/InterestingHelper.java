package ToxicNaga.Naiakoa.Nexion.Handlers;

import cpw.mods.fml.relauncher.ReflectionHelper;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.util.ResourceLocation;

/**
 * Created by Naiakoa on 2/6/2015.
 */
public class InterestingHelper {

    public static ResourceLocation getParticleTexture() {
        return ReflectionHelper.getPrivateValue(EffectRenderer.class, null, "particleTextures", "field_110737_b", "b");
    }
}