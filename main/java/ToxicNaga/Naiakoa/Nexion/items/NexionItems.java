package ToxicNaga.Naiakoa.Nexion.items;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.items.Biode.BiodeElement;
import ToxicNaga.Naiakoa.Nexion.items.Other.DiamondRedstone;
import ToxicNaga.Naiakoa.Nexion.items.Wurtzite.*;
import ToxicNaga.Naiakoa.Nexion.items.Wurtzite.WurtziteDiamondShard;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class NexionItems extends Item {
    //public static final NexionItemRegistry DefineName = new ClassCall();

    /* Begin Mod Item */
    public static final NexionItemRegistry DiamondRedstone = new DiamondRedstone();
    
    /* Wurtzite Items */
    public static final NexionItemRegistry WurtziteFragment = new WurtziteFragment();
    public static final NexionItemRegistry WurtziteShard = new WurtziteShard();
    public static final NexionItemRegistry WurtziteDiamondShard = new WurtziteDiamondShard();
    public static final NexionItemRegistry WurtziteCrystal = new WurtziteCrystal();

    /* Elements */
    public static final NexionItemRegistry WurtziteElement = new WurtziteElement();
    public static final NexionItemRegistry BiodeElement = new BiodeElement();

    /* Biode Items */

    /* Nexion Items */

    /* Lore Book Items */

    public static void init(){
        //GameRegistry.registerItem(ClassName, Language Tag Name);
        GameRegistry.registerItem(DiamondRedstone, NexionStrings.RedDiamond);
        
        GameRegistry.registerItem(WurtziteFragment, NexionStrings.Wurtzite_Fragment);
        GameRegistry.registerItem(WurtziteShard, NexionStrings.Wurtzite_Shard);
        GameRegistry.registerItem(WurtziteDiamondShard, NexionStrings.WurtziteDiamondShard); //You Smelt this into Wurtzite Crystal
        GameRegistry.registerItem(WurtziteCrystal, NexionStrings.Wurtzite_Crystal);
        GameRegistry.registerItem(WurtziteElement, NexionStrings.Wurtzite_Element);
        GameRegistry.registerItem(BiodeElement, NexionStrings.Biode_Element);
    }
}
