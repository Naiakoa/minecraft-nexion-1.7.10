package ToxicNaga.Naiakoa.Nexion.items.Wurtzite;

import ToxicNaga.Naiakoa.Nexion.CreativeTabs.NexionTabElements;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;

/**
 * Created by Naiakoa on 1/28/2015.
 */
public class WurtziteElement extends NexionItemRegistry {
    public WurtziteElement()
    {
        super();
        this.setUnlocalizedName(NexionStrings.Wurtzite_Element);
        this.setCreativeTab(Nexion.nElements);
    }
}
