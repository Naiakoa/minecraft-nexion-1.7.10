package ToxicNaga.Naiakoa.Nexion.items.Wurtzite;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;
import ToxicNaga.Naiakoa.Nexion.items.NexionItems;

/**
 * Created by Naiakoa on 1/29/2015.
 */
public class WurtziteShard extends NexionItemRegistry {
    public WurtziteShard(){
        super();
        this.setUnlocalizedName(NexionStrings.Wurtzite_Shard);
        this.setCreativeTab(Nexion.nItems);
        this.maxStackSize = 64;
    }
}
