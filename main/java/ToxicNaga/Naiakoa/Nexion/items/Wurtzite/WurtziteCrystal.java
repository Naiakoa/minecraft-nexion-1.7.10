package ToxicNaga.Naiakoa.Nexion.items.Wurtzite;

import ToxicNaga.Naiakoa.Nexion.Blocks.DungeonBlocks.DungeonMechanic;
import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.common.entity.BiodeGuardian.EntityFireLord;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * Created by Naiakoa on 1/29/2015.
 */
public class WurtziteCrystal extends NexionItemRegistry{
    public WurtziteCrystal(){
        super();
        this.setUnlocalizedName(NexionStrings.Wurtzite_Crystal);
        this.setCreativeTab(Nexion.nItems);
        this.maxStackSize = 64;
    }

    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
    {
        DungeonMechanic.BossDead = false;
        return EntityFireLord.spawn(par2EntityPlayer, par1ItemStack, par3World, par4, par5, par6, false);
    }
}