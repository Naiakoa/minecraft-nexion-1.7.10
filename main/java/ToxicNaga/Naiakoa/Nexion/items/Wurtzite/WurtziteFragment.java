package ToxicNaga.Naiakoa.Nexion.items.Wurtzite;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;
import ToxicNaga.Naiakoa.Nexion.util.achievement.IPickupAchievement;
import ToxicNaga.Naiakoa.Nexion.util.achievement.NexionAchievements;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

/**
 * Created by Naiakoa on 1/29/2015.
 */
public class WurtziteFragment extends NexionItemRegistry implements IPickupAchievement{


    public WurtziteFragment(){
        super();
        this.setUnlocalizedName(NexionStrings.Wurtzite_Fragment);
        this.setCreativeTab(Nexion.nItems);
        this.maxStackSize = 64;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public Achievement getAchievementOnPickup(ItemStack stack, EntityPlayer player, EntityItem item) {

        if(!((EntityPlayerMP)player).func_147099_x().hasAchievementUnlocked(NexionAchievements.wurtzitePickup)){
            player.worldObj.playSoundAtEntity(player,"nexion:achieveGain",3F,1F);
        }
        return NexionAchievements.wurtzitePickup;
    }
}
