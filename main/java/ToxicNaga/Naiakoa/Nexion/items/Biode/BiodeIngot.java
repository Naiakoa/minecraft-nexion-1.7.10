package ToxicNaga.Naiakoa.Nexion.items.Biode;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;
import ToxicNaga.Naiakoa.Nexion.items.NexionItems;

/**
 * Created by Naiakoa on 2/13/2015.
 */
public class BiodeIngot extends NexionItemRegistry {
    public BiodeIngot()
    {
        super();
        this.setUnlocalizedName(NexionStrings.Biode_Ingot);
        this.setCreativeTab(Nexion.nItems);
    }
}