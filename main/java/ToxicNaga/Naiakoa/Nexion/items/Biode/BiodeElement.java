package ToxicNaga.Naiakoa.Nexion.items.Biode;

import ToxicNaga.Naiakoa.Nexion.Handlers.NexionStrings;
import ToxicNaga.Naiakoa.Nexion.Nexion;
import ToxicNaga.Naiakoa.Nexion.items.NexionItemRegistry;

/**
 * Created by Naiakoa on 2/13/2015.
 */
public class BiodeElement extends NexionItemRegistry {
    public BiodeElement()
    {
        super();
        this.setUnlocalizedName(NexionStrings.Biode_Element);
        this.setCreativeTab(Nexion.nElements);
    }
}
